﻿namespace Axiobat.Services
{
    using App.Common;
    using Application.Data;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.Mission;
    using AutoMapper;
    using Axiobat.Application.Exceptions;
    using Axiobat.Application.Models;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation for <see cref="IMissionService"/>
    /// </summary>
    public partial class MissionService
    {
        /// <summary>
        /// get paged result of the missions types
        /// </summary>
        /// <param name="filterOptions">the filter option to be used for retrieving the list of mission types</param>
        /// <returns>the paged result of the mission types</returns>
        public async Task<PagedResult<MissionTypeModel>> GetMissionTypesAsync(MissionTypeFilterOptions filterOptions)
        {
            var data = await _missionTypeDataAccess.GetPagedResultAsync(filterOptions);
            var result = _mapper.Map<MissionTypeModel[]>(data.Value);
            return Result.PagedSuccess(result, data);
        }

        /// <summary>
        /// create or update a mission type, based on the given model
        /// </summary>
        /// <param name="model">the model to be used when creating or updating a mission type</param>
        /// <returns>the updated/created mission type</returns>
        public async Task<Result<MissionTypeModel>> PutMissionTypeAsync(MissionTypeModel model)
        {
            var entity = await _missionTypeDataAccess.GetByIdAsync(model.Id);

            // check if requesting creation
            if (entity is null || model.Id == -1)
            {
                var entityFromModel = _mapper.Map<MissionType>(model);
                var addResult = await _missionTypeDataAccess.AddAsync(entityFromModel);
                if (!addResult.IsSuccess)
                    return Result.From<MissionTypeModel>(addResult);

                return _mapper.Map<MissionTypeModel>(addResult.Value);
            }

            // the entity not exist
            if (entity is null)
                return Result.Failed<MissionTypeModel>($"Failed to retrieve the MissionType with id: {model.Id}");

            // update the entity
            model.Update(entity);
            var updateResult = await _missionTypeDataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.From<MissionTypeModel>(updateResult);

            // return the result
            return _mapper.Map<MissionTypeModel>(updateResult.Value);
        }

        /// <summary>
        /// delete the mission type with the given id
        /// </summary>
        /// <param name="missionTypeId">the id of the mission type</param>
        public async Task DeleteMissionTypeAsync(int missionTypeId)
        {
            var entity = await _missionTypeDataAccess.GetByIdAsync(missionTypeId);

            if (entity is null)
                throw new NotFoundException($"Failed to retrieve the MissionType with id: {missionTypeId}");

            await _missionTypeDataAccess.DeleteAsync(entity);
        }
    }

    /// <summary>
    /// partial part for <see cref="MissionService"/>
    /// </summary>
    public partial class MissionService : DataService<Mission, string>, IMissionService
    {
        private readonly IMissionTypeDataAccess _missionTypeDataAccess;

        public MissionService(
            IMissionTypeDataAccess missionTypeDataAccess,
            IMissionDataAccess dataAccess,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            this._missionTypeDataAccess = missionTypeDataAccess;
        }

        protected override Task InCreate_BeforInsertAsync<TCreateModel>(Mission entity, TCreateModel createModel)
        {
            if (!entity.WorkshopId.IsValid())
                entity.WorkshopId = null;

            if (!entity.ClientId.IsValid())
                entity.ClientId = null;

            return base.InCreate_BeforInsertAsync(entity, createModel);
        }

        protected override Task InUpdate_BeforUpdateAsync<TUpdateModel>(Mission entity, TUpdateModel updateModel)
        {
            entity.Client = null;
            entity.Technician = null;

            if (!entity.WorkshopId.IsValid())
                entity.WorkshopId = null;

            if (!entity.ClientId.IsValid())
                entity.ClientId = null;

            return Task.CompletedTask;
        }
    }
}
