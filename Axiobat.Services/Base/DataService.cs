﻿namespace Axiobat.Services
{
    using Application.Data;
    using Application.Models;
    using Application.Services;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using AutoMapper;
    using Application.Exceptions;
    using Domain.Interfaces;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Axiobat.Application.Services.Localization;

    /// <summary>
    /// this class is the base class for all data service
    /// </summary>
    /// <typeparam name="TKey">the type of the entity key</typeparam>
    /// <typeparam name="TEntity">the type of the entity, should be a class that implement <see cref="IEntity{Tkey}"/></typeparam>
    public partial class DataService<TEntity, TKey>
    {
        /// <summary>
        /// this method is used to create a recode for of the entity
        /// </summary>
        /// <typeparam name="TOut">the type of the out put, useful if you want to return a deferent type than the model</typeparam>
        /// <typeparam name="TCreateModel">the type of the create model</typeparam>
        /// <param name="createModel">the create model</param>
        /// <returns>a result operation, containing the newly created entity</returns>
        /// <remarks>
        /// the creation process is consist of multiple phases
        /// -1 : Conversion from the model to the entity type
        /// -2 : Validating the document
        /// -3 : Supply Additional dataToEntity
        /// -4 : insert entity to database using the dataAccess provider
        /// -5 : convert the entity to the desired output type
        /// </remarks>
        public virtual async Task<Result<TOut>> CreateAsync<TOut, TCreateModel>(TCreateModel createModel)
            where TCreateModel : class
        {
            var entity = _mapper.Map<TEntity>(createModel);

            /* Hook : validating the entity */
            await InCreate_ValidateEntityAsync(entity);

            /* Hook : before inserting into database */
            await InCreate_BeforInsertAsync(entity, createModel);

            // recored history
            RecordInsertChangesHistory(entity);

            var addResult = await _dataAccess.AddAsync(entity);
            if (!addResult.IsSuccess)
            {
                _logger.LogWarning(LogEvent.InsertARecored, "Failed adding {entityType}, check previous logs", Entity);
                return Result.From<TOut>(addResult);
            }

            /* Hook : after inserting the entity */
            await InCreate_AfterInsertAsync(entity, createModel);

            // generate a reference for the entity
            await IncrementReferenceOnCreateAsync(entity);

            // map and return the created entity
            return _mapper.Map<TOut>(addResult.Value);
        }

        /// <summary>
        /// this method is used to update the entity white the given id, using the update model
        /// </summary>
        /// <typeparam name="TOut">type of the output, the type should be registered with auto mapper</typeparam>
        /// <param name="id">the id of the entity to be updated</param>
        /// <param name="createModel">the create model</param>
        /// <returns>the desired type</returns>
        public virtual async Task<Result<TOut>> UpdateAsync<TOut, TUpdateModel>(TKey id, TUpdateModel updateModel)
            where TUpdateModel : class, IUpdateModel<TEntity>
        {
            // get the entity by id
            var entity = await _dataAccess.GetByIdAsync(id);
            if (entity is null)
                throw new NotFoundException($"the '{Entity}' not exist");

            // the old state before the update
            var oldState = (TEntity)entity.Clone();

            /* Hook: validating the entity */
            await InUpdate_ValidateEntityAsync(entity, updateModel);

            // recored changes history
            RecordUpdateChangesHistory(entity, updateModel);

            // update entity from the model
            updateModel.Update(entity);

            /* Hook : before inserting into database */
            await InUpdate_BeforUpdateAsync(entity, updateModel);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.From<TOut>(updateResult);

            /* Hook : before inserting into database */
            await InUpdate_AfterUpdateAsync(entity, updateModel);

            /*increment reference*/
            await IncrementReferenceOnUpdateAsync(entity, oldState);

            _logger.LogInformation(LogEvent.UpdatingData, "Entity Updated Successfully");
            return _mapper.Map<TOut>(updateResult.Value);
        }

        /// <summary>
        /// delete the entity with the given id
        /// </summary>
        /// <param name="id">the id of the entity to be deleted</param>
        /// <returns>the deletion operation result</returns>
        public virtual async Task<Result> DeleteAsync(TKey id)
        {
            var entity = await _dataAccess.GetByIdAsync(id);
            if (entity is null)
                throw new NotFoundException($"the '{Entity}' not exist");

            /* Hook : before deleting the entity */
            await InDelete_BeforDeleteAsync(entity);

            // recored the changes history
            RecordDeleteChangesHistory(entity);

            // delete the entity
            var deleteResult = await DeleteEntityAsync(entity);
            if (!deleteResult.IsSuccess)
                return deleteResult;

            /*hook: after a successful delete*/
            await InDelete_AfterDeleteAsync(entity);

            return deleteResult;
        }

        /// <summary>
        /// get the entity with the given id
        /// </summary>
        /// <param name="id">the id of the entity to retrieve</param>
        /// <returns>the result, containing the entity</returns>
        public virtual async Task<Result<TOut>> GetByIdAsync<TOut>(TKey id)
        {
            var entity = await _dataAccess.GetByIdAsync(id);
            if (entity is null)
                throw new NotFoundException($"the '{Entity}' not exist");

            var mappedEntity = _mapper.Map<TOut>(entity);

            /*hook after mapping the entity*/
            await InGet_AfterMappingAsync(entity, mappedEntity);

            return mappedEntity;
        }

        /// <summary>
        /// get the list of all <see cref="TEntity"/>
        /// </summary>
        /// <returns>the result of desired type</returns>
        public virtual async Task<ListResult<TOut>> GetAllAsync<TOut>()
            => _mapper.Map<List<TOut>>(await _dataAccess.GetAllAsync(null));

        /// <summary>
        /// get list of all entities by the given identifiers
        /// </summary>
        /// <typeparam name="TOut">type of the output</typeparam>
        /// <param name="identifiers">list of identifiers</param>
        /// <returns>list of entities</returns>
        public virtual async Task<TOut[]> GetAllAsync<TOut>(params TKey[] identifiers)
            => _mapper.Map<TOut[]>(await _dataAccess.GetByIdAsync(identifiers));

        /// <summary>
        /// get the list of the entities as paged result
        /// </summary>
        /// <param name="filterOption">the pagination options</param>
        /// <returns>the paged result</returns>
        public virtual async Task<PagedResult<TOut>> GetAsPagedResultAsync<TOut, IFilter>(IFilter filterOption)
            where IFilter : IFilterOptions
        {
            /* Hook: before retrieving the result */
            await InPagedResult_BeforeListRetrievalAsync(filterOption);

            // retrieve paged list result
            var result = await _dataAccess.GetPagedResultAsync(filterOption);

            // check the result
            if (!result.IsSuccess)
                return Result.ListFrom<TOut, TEntity>(result);

            // map and return the result
            return Result.PagedSuccess(_mapper.Map<List<TOut>>(result.Value), result);
        }

        /// <summary>
        /// check if the entity with the given id exist or not
        /// </summary>
        /// <param name="entityId">the id of the entity</param>
        /// <returns>true if exist false if not</returns>
        public virtual Task<bool> IsExistAsync(TKey entityId)
            => _dataAccess.IsExistAsync(entityId);

        /// <summary>
        /// export the data using the given options
        /// </summary>
        /// <param name="exportOptions">the export options</param>
        /// <returns>the file as Byte array</returns>
        public async Task<Result<byte[]>> ExportDataAsync<TExportOption>(TExportOption exportOptions)
            where TExportOption : DataExportOptions
        {
            var excelFile = _fileService.GenerateFile(await _dataAccess.GetAllAsync(null), exportOptions);
            if (excelFile is null)
            {
                _logger.LogWarning(LogEvent.ExportData, "Failed to export the data, check previous logs");
                return Result.Failed<byte[]>($"Failed to export the data");
            }

            return excelFile;
        }

        /// <summary>
        /// export data of the entity with the given id
        /// </summary>
        /// <typeparam name="TExportOption">the type of the export options</typeparam>
        /// <typeparam name="Tkey">the type of the entity key</typeparam>
        /// <param name="entityId">the id of the entity to export</param>
        /// <param name="exportOptions">the export options</param>
        /// <returns>the file as a byte[]</returns>
        public async Task<Result<byte[]>> ExportDataAsync<TExportOption>(TKey entityId, TExportOption exportOptions)
            where TExportOption : DataExportOptions
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("there is no entity with the given id");

            var excelFile = _fileService.GenerateFile(entity, exportOptions);
            if (excelFile is null)
            {
                _logger.LogWarning(LogEvent.ExportData, "Failed to export the data, check previous logs");
                return Result.Failed<byte[]>($"Failed to export the data");
            }

            return excelFile;
        }
    }

    /// <summary>
    /// the partial part of <see cref="DataService{TKey, TEntity, TModel, TUpdateModel, TCreateModel}"/>
    /// </summary>
    public partial class DataService<TEntity, TKey> : BaseService, IDataService<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        protected bool RecordHistroyChanges = true;

        /// <summary>
        /// the name of the entity
        /// </summary>
        protected override string EntityName => typeof(TEntity).Name;

        /// <summary>
        /// Name of the entity, this only will give you a string value of the type of the entity
        /// </summary>
        protected string Entity => typeof(TEntity).Name;

        /// <summary>
        /// the ensure service to handle subscription requirements
        /// </summary>
        protected readonly IValidationService _validate;

        /// <summary>
        /// the history service
        /// </summary>
        protected readonly IHistoryService _history;

        /// <summary>
        /// file service
        /// </summary>
        protected readonly IFileService _fileService;

        /// <summary>
        /// the data Access instant
        /// </summary>
        protected readonly IDataAccess<TEntity, TKey> _dataAccess;

        public DataService(
            IDataAccess<TEntity, TKey> dataAccess,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _dataAccess = dataAccess;
            _validate = validation;
            _history = historyService;
            _fileService = fileService;
        }

        /// <summary>
        /// Record the Insert action Changes History of the entity
        /// </summary>
        /// <param name="entity">the entity</param>
        private void RecordInsertChangesHistory(TEntity entity)
        {
            if (entity is IRecordable recordable && RecordHistroyChanges)
                _history.Recored(recordable, Domain.Enums.ChangesHistoryType.Added);
        }

        /// <summary>
        /// Record the Insert action Changes History of the entity
        /// </summary>
        /// <param name="entity">the entity</param>
        private void RecordDeleteChangesHistory(TEntity entity)
        {
            if (entity is IRecordable recordable && RecordHistroyChanges)
                _history.Recored(recordable, Domain.Enums.ChangesHistoryType.Deleted);
        }

        /// <summary>
        /// recored the update operation changes
        /// </summary>
        /// <typeparam name="TUpdateModel">the type of the update model</typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="updateModel">the update model instant</param>
        private void RecordUpdateChangesHistory<TUpdateModel>(TEntity entity, TUpdateModel updateModel)
            where TUpdateModel : class, IUpdateModel<TEntity>
        {
            if (entity is IRecordable recordable && RecordHistroyChanges)
                _history.RecoredUpdate(recordable, updateModel);
        }

        /// <summary>
        /// generate a reference for the entity if it is a referenceable entity
        /// </summary>
        private async Task IncrementReferenceOnCreateAsync(TEntity entity)
        {
            // entity has a reference
            if (entity is IReferenceable<TEntity> referenceableEntity)
            {
                if (referenceableEntity.CanIncrementOnCreate())
                    await _configuration.IncrementReferenceAsync(typeof(TEntity).DocType());
            }
        }

        /// <summary>
        /// generate a reference for the entity if it is a referenceable entity
        /// </summary>
        private async Task IncrementReferenceOnUpdateAsync(TEntity entity, TEntity oldVersion)
        {
            // entity has a reference
            if (entity is IReferenceable<TEntity> referenceableEntity)
            {
                if (referenceableEntity.CanIncrementOnUpdate(oldVersion))
                    await _configuration.IncrementReferenceAsync(typeof(TEntity).DocType());
            }
        }

        /// <summary>
        /// delete the entity by implementing the delete logic associated with each entity (override this method to set your own deletion logic)
        /// this method uses the DeleteAsync Method on the DataAccess which means that the entity will be deleted permanently
        /// </summary>
        /// <param name="entity">the entity to delete</param>
        /// <returns>the operation result</returns>
        protected virtual async Task<Result> DeleteEntityAsync(TEntity entity)
        {
            if (entity is IDeletable deletable)
            {
                deletable.IsDeleted = true;

                // update the entity
                var result = await _dataAccess.UpdateAsync(entity);
                if (!result.IsSuccess)
                    return result;

                return Result.Success();
            }

            return await _dataAccess.DeleteAsync(entity);
        }

        /// <summary>
        /// use this method to apply all necessary validation to the entity
        /// if one of your validation has failed throw an exception of type <see cref="ArtinoveValidationException"/>
        /// if everything is good and nothing failed just return;
        /// </summary>
        /// <param name="entity">the entity instant to validate</param>
        protected virtual Task InCreate_ValidateEntityAsync(TEntity entity)
            => Task.CompletedTask;

        /// <summary>
        /// this hook will be called before the entity is inserted to the database
        /// </summary>
        /// <typeparam name="TCreateModel">the create model type</typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="createModel">the entity creation model</param>
        protected virtual Task InCreate_BeforInsertAsync<TCreateModel>(TEntity entity, TCreateModel createModel) where TCreateModel : class
            => Task.CompletedTask;

        /// <summary>
        /// this hook will be called after the entity is inserted to the database successfully
        /// </summary>
        /// <typeparam name="TCreateModel">the create model type</typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="createModel">the entity creation model</param>
        protected virtual Task InCreate_AfterInsertAsync<TCreateModel>(TEntity entity, TCreateModel createModel) where TCreateModel : class
            => Task.CompletedTask;

        /// <summary>
        /// use this method to apply all necessary validation to the entity
        /// if one of your validation has failed throw an exception of type <see cref="ArtinoveValidationException"/>
        /// if everything is good and nothing failed just return;
        /// </summary>
        /// <param name="entity">the entity instant to validate</param>
        protected virtual Task InUpdate_ValidateEntityAsync(TEntity entity, IUpdateModel<TEntity> updateModel)
            => Task.CompletedTask;

        /// <summary>
        /// this hook will be called before the entity is updated in database
        /// </summary>
        /// <param name="entity">the entity instant</param>
        protected virtual Task InUpdate_BeforUpdateAsync<TUpdateModel>(TEntity entity, TUpdateModel updateModel) where TUpdateModel : class, IUpdateModel<TEntity>
            => Task.CompletedTask;

        /// <summary>
        /// this hook will be called after the entity is updated in database
        /// </summary>
        /// <param name="entity">the entity instant</param>
        protected virtual Task InUpdate_AfterUpdateAsync<TUpdateModel>(TEntity entity, TUpdateModel updateModel) where TUpdateModel : class, IUpdateModel<TEntity>
            => Task.CompletedTask;

        /// <summary>
        /// use this method to apply all necessary validation to the entity
        /// if one of your validation has failed throw an exception of type <see cref="ArtinoveValidationException"/>
        /// if everything is good and nothing failed just return;
        /// </summary>
        /// <param name="entity">the entity instant to validate</param>
        protected virtual Task InDelete_BeforDeleteAsync(TEntity entity)
            => Task.CompletedTask;

        /// <summary>
        /// after a successful delete of an entity
        /// </summary>
        /// <param name="entity">the entity instant</param>
        protected virtual Task InDelete_AfterDeleteAsync(TEntity entity)
            => Task.CompletedTask;

        /// <summary>
        /// after mapping the entity
        /// </summary>
        /// <typeparam name="TOut">the output Type</typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="mappedEntity">the mapped entity instant</param>
        protected virtual Task InGet_AfterMappingAsync<TOut>(TEntity entity, TOut mappedEntity)
            => Task.CompletedTask;

        /// <summary>
        /// before the paged list retrieved
        /// </summary>
        /// <typeparam name="IFilter">the type of the filter</typeparam>
        /// <param name="filterOption">the filter options instant</param>
        protected virtual Task InPagedResult_BeforeListRetrievalAsync<IFilter>(IFilter filterOption)
            where IFilter : IFilterOptions => Task.CompletedTask;
    }
}
