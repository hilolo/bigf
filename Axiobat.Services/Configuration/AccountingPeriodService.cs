﻿namespace Axiobat.Services.Configuration
{
    using Application.Data;
    using Application.Exceptions;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implantation for <see cref="IAccountingPeriodService"/>
    /// </summary>
    public partial class AccountingPeriodService
    {
        /// <summary>
        /// get the current accounting period
        /// </summary>
        /// <returns>the starting date of the period, and the ending of the period</returns>
        public async Task<TOut> GetCurrentAccountingPeriodAsync<TOut>()
        {
            var result = await _dataAccess.CurrentAccountingPeriodAsync();
            return _mapper.Map<TOut>(result);
        }

        /// <summary>
        /// get the accounting Period that will end today
        /// </summary>
        /// <typeparam name="TOut">the type of the output</typeparam>
        /// <returns>the accounting period</returns>
        public Task<TOut> GetAccountingPeriodEndsTodayAsync<TOut>()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// close the accounting period with the given id
        /// </summary>
        /// <param name="accountingPeriodId">the id of the accounting period</param>
        public async Task CloseAccountingPeriodAsync(int accountingPeriodId)
        {
            var accountingPeriod = await _dataAccess.GetByIdAsync(accountingPeriodId);
            if (accountingPeriod is null)
                throw new NotFoundException("the given accounting period not exist"); //

            if (accountingPeriod.CalculateEndingDate() > DateTime.Now.AddDays(-5))
                throw new ValidationException("the selected accounting period has not reached the ending of it period", MessageCode.AccountingPeriodHasNotReachedTheEndingYet);

            accountingPeriod.EndingDate = DateTime.Now;
            accountingPeriod.UserId = (await _loggedInUserService.GetCurrentUserOrAdminAsync())?.Id;

            var updateResult = await _dataAccess.UpdateAsync(accountingPeriod);
            if (!updateResult.IsSuccess)
            {
                _logger.LogError("Failed to close the accounting period with id [{id}]", accountingPeriodId);
                throw new AxiobatInternalException("Failed to update accounting period");
            };

            var addResult = await _dataAccess.AddAsync(new AccountingPeriod
            {
                Period = accountingPeriod.Period,
                StartingDate = accountingPeriod.CalculateEndingDate(),
                UserId = (await _loggedInUserService.GetCurrentUserOrAdminAsync())?.Id,
            });
            if (!addResult.IsSuccess)
            {
                _logger.LogError("Failed to add the new accounting period", accountingPeriodId);
                throw new AxiobatInternalException("Failed to add the new accounting period");
            };
        }

        /// <summary>
        /// close the current accounting period
        /// </summary>
        public async Task CloseCurrentAccountingPeriodAsync()
        {
            var accountingPeriod = await _dataAccess.CurrentAccountingPeriodAsync();
            if (accountingPeriod is null)
                throw new NotFoundException("there is no accounting period defined, define one in the application configuration");

            await CloseAccountingPeriodAsync(accountingPeriod.Id);
        }

        /// <summary>
        /// this function will check if the given date is in this accounting period
        /// </summary>
        /// <param name="date">the date to be checked</param>
        /// <returns>true if in the current accounting period, false if not</returns>
        public async Task<bool> IsInCurrentAccountingPeriod(DateTime date)
        {
            var currentAccountingPeriod = await GetCurrentAccountingPeriodAsync<AccountingPeriod>();
            return currentAccountingPeriod.InAccountingPeriod(date);
        }
    }

    /// <summary>
    /// partial part for <see cref="AccountingPeriodService"/>
    /// </summary>
    public partial class AccountingPeriodService : DataService<AccountingPeriod, int>, IAccountingPeriodService
    {
        private new IAccountingPeriodDataAccess _dataAccess => base._dataAccess as IAccountingPeriodDataAccess;

        public AccountingPeriodService(
            IAccountingPeriodDataAccess dataAccess,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
        }
    }
}
