﻿namespace Axiobat.Services.Configuration
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Localization;
    using AutoMapper;
    using Axiobat.Application;
    using Domain.Entities;
    using Domain.Enums;
    using Domain.Interfaces;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation of <see cref="IHistoryService"/>
    /// </summary>
    public partial class HistoryService
    {
        /// <summary>
        /// add a new global history record with the given type
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity being manipulated</typeparam>
        /// <param name="actionType">the type of the action</param>
        public Task AddAsync<TEntity>(ChangesHistoryType actionType)
            => AddAsync<TEntity>(actionType, _loggedInUserService.User.UserId);

        /// <summary>
        /// add a new global history record with the given type
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity being manipulated</typeparam>
        /// <param name="actionType">the type of the action</param>
        /// <param name="companyId">the id of the company</param>
        /// <param name="userId">the id of the user</param>
        public Task AddAsync<TEntity>(ChangesHistoryType actionType, Guid userId)
        {
            var history = new GlobalHistory
            {
                UserId = userId,
                Action = actionType,
                ActionDate = DateTime.UtcNow,
                DocumentType = typeof(TEntity).DocType(),
                DocumentId = "",
            };

            return _globalHistoryDataAccess.AddAsync(history);
        }

        /// <summary>
        /// recored a new changes history to the given entity
        /// </summary>
        /// <typeparam name="TEntity">the entity type, must implement <see cref="IRecordable"/></typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="ChangesHistory">the changes history information</param>
        public void Recored<TEntity>(TEntity entity, Action<ChangesHistoryOptions> ChangesHistory) where TEntity : IRecordable
        {
            // create the options
            var options = new ChangesHistoryOptions();

            // set the options
            ChangesHistory(options);

            // add the history record
            Recored(entity, options.Action, options.Information, options.Fields);
        }

        /// <summary>
        /// recored a changes history
        /// </summary>
        /// <param name="entity">the entity implementing <see cref="IRecordable"/></param>
        /// <param name="actionType">the action type</param>
        /// <param name="information">the information</param>
        /// <param name="fields">the entity champs changes</param>
        public void Recored<TEntity>(TEntity entity, ChangesHistoryType actionType, string information = null, ICollection<ChangedField> fields = null) where TEntity : IRecordable
            => Recored(entity.ChangesHistory, actionType, _loggedInUserService.GetMinimalUser(), information, fields);

        /// <summary>
        /// recored a changes history
        /// </summary>
        /// <param name="history">the entity history collection</param>
        /// <param name="actionType">the action type</param>
        /// <param name="fields">the entity champs changes</param>
        public void Recored(List<ChangesHistory> history, ChangesHistoryType actionType, string information = null, ICollection<ChangedField> fields = null)
            => Recored(history, actionType, _loggedInUserService.GetMinimalUser(), information, fields);

        /// <summary>
        /// recored a changes history
        /// </summary>
        /// <param name="history">the entity history collection</param>
        /// <param name="actionType">the action type</param>
        /// <param name="user">the user how made the change</param>
        /// <param name="fields">the entity champs changes</param>
        public void Recored(List<ChangesHistory> history, ChangesHistoryType actionType, MinimalUser user, string information = null, ICollection<ChangedField> fields = null)
        {
            // if the action type is update, than check for fields data
            if (actionType == ChangesHistoryType.Updated && !(fields is null) && fields.Count <= 0)
                return;

            // add the history record
            history.Insert(0, new ChangesHistory
            {
                User = user,
                Action = actionType,
                DateAction = DateTime.Now,
                Information = information,
                Fields = fields
            });
        }

        /// <summary>
        /// detect the changes and add them to the history of the associate how is updated
        /// </summary>
        /// <param name="model">the update model, with all new values</param>
        /// <param name="entity">the associate in database</param>
        public void RecoredUpdate<TEntity, TModel>(TEntity entity, TModel model) where TEntity : IRecordable
            => Recored(entity.ChangesHistory, ChangesHistoryType.Updated, null, GetChangedFields(_mapper.Map<TModel>(entity), model));

        /// <summary>
        /// a method to detect the fields that has been changed between the current and the updated value
        /// </summary>
        /// <typeparam name="T">the type of the value to compare</typeparam>
        /// <param name="current">the current value or the original value</param>
        /// <param name="updated">the updated or the new value</param>
        /// <returns>the list of changes</returns>
        public List<ChangedField> GetChangedFields<T>(T current, T updated)
        {
            var changesList = new List<ChangedField>();

            foreach (var property in typeof(T).GetProperties())
            {
                var currentValue = property.GetValue(current);
                var updatedValue = property.GetValue(updated);

                if (Equals(currentValue, updatedValue) || property.HasAttribute<IgnorePropertyAttribute>() || IsComplexType(property))
                    continue;

                if (property.PropertyType.IsClass && !(property.PropertyType == typeof(string)))
                {
                    changesList.AddRange(GetChangedFields(currentValue, updatedValue));
                    continue;
                }

                changesList.Add(new ChangedField()
                {
                    PropName = property.Name,
                    Champ = GetPropertyName(property),
                    ValeurInitial = GetInitialValue(property, currentValue, current),
                    ValeurFinal = GetFinalValue(property, updatedValue, updated),
                });
            }

            return changesList;
        }
    }

    /// <summary>
    /// partial part for <see cref="HistoryService"/>
    /// </summary>
    public partial class HistoryService : BaseService, IHistoryService
    {
        private readonly IGlobalHistoryDataAccess _globalHistoryDataAccess;

        protected override string EntityName => typeof(GlobalHistory).Name;

        public HistoryService(
            IGlobalHistoryDataAccess globalHistoryDataAccess,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appConfigService,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(loggedInUserService, appConfigService, transalationService, loggerFactory, mapper)
        {
            _globalHistoryDataAccess = globalHistoryDataAccess;
        }

        /// <summary>
        /// ensure a value for the given object
        /// </summary>
        /// <param name="value">the value of the object</param>
        /// <returns>the string value of the object</returns>
        public string GetInitialValue<TModel>(PropertyInfo property, dynamic value, TModel current)
        {
            var customValue = TryGetPropValue(property, current);
            if (customValue is null)
            {
                if (value is null || IsList(value))
                    return "";

                return value.ToString();
            }

            return customValue.ToString();
        }

        private string GetFinalValue<TModel>(PropertyInfo property, object value, TModel updated)
        {
            var customValue = TryGetPropValue(property, updated);
            if (customValue is null)
            {
                if (value is null || IsList(value))
                    return "";

                return value.ToString();
            }

            return customValue.ToString();
        }

        /// <summary>
        /// get the proper name of the property
        /// </summary>
        /// <param name="property">the property info object</param>
        /// <returns>a string value that represent the name of the property</returns>
        public string GetPropertyName(PropertyInfo property)
        {
            var propertyName = property.Name;

            if (property.GetCustomAttributes(typeof(PropertyNameAttribute), true).FirstOrDefault() is PropertyNameAttribute propertyNameAttribute)
                propertyName = propertyNameAttribute.PropertyName;

            return T(propertyName);
        }

        /// <summary>
        /// check if the property is a complex type that is built using JSON, it checks the existence
        /// of the ComplexType Attribute on the property
        /// </summary>
        /// <param name="property">the property to check</param>
        /// <returns>true if complex type</returns>
        private bool IsComplexType(PropertyInfo property)
        {
            var complexTypeAttribute = property
               .GetCustomAttributes(typeof(ComplexTypeAttribute), true)
               .FirstOrDefault();

            if (!(complexTypeAttribute is null))
                return true;

            return false;
        }

        /// <summary>
        /// check if the given object is a list
        /// </summary>
        /// <param name="dynamic">the dynamic instant of the object</param>
        /// <returns>true if a list, false if not</returns>
        private bool IsList(dynamic dynamic)
            => dynamic is string ? false : dynamic is System.Collections.IEnumerable;

        private static object TryGetPropValue<TModel>(PropertyInfo property, TModel current)
        {
            if (property.GetCustomAttributes(typeof(PropertyValueAttribute), true).FirstOrDefault() is PropertyValueAttribute attribute)
            {
                object value = current;

                foreach (var item in attribute.NavigationProprety.Split('.'))
                {
                    var objectType = value.GetType();

                    var propInfo = objectType
                        .GetProperty(item);

                    if (propInfo is null)
                        return null;

                    value = propInfo.GetValue(value, null);
                }

                return value;
            }

            return null;
        }
    }
}
