﻿namespace Axiobat.Services.Products
{
    using Application.Data;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.Products;
    using AutoMapper;
    using Domain.Entities;
    using Domain.Exceptions;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// service implantation for <see cref="ILotService"/>
    /// </summary>
    public partial class LotService
    {
        /// <summary>
        /// check if the given lot name is unique or not
        /// </summary>
        /// <param name="designation">the name of the lot to be checked</param>
        /// <returns>true if unique false if not</returns>
        public async Task<bool> IsNameUniqueAsync(string designation)
            => !await _dataAccess.IsExistAsync(e => e.Designation == designation);

        /// <summary>
        /// get the list of products that lo owns
        /// </summary>
        /// <param name="lotId">the id of the lot</param>
        /// <returns>list of products</returns>
        public async Task<ListResult<ProductModel>> GetLotProductsAsync(string lotId)
        {
            var lotProducts = await _dataAccess.GetLotProducts(lotId);
            return Result.ListSuccess(_mapper.Map<IEnumerable<ProductModel>>(lotProducts));
        }
    }

    /// <summary>
    /// partial part for <see cref="LotService"/>
    /// </summary>
    public partial class LotService : ProductsBaseService<Lot>, ILotService
    {
        protected new ILotDataAccess _dataAccess => base._dataAccess as ILotDataAccess;

        public LotService(
            ILotDataAccess dataAccess,
            ISynchronizationResolverService synchronizationResolverService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolverService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
        }


        /// <summary>
        /// before updating the lot entity
        /// </summary>
        /// <typeparam name="TUpdateModel">the update model</typeparam>
        /// <param name="entity">the lot entity instant</param>
        /// <param name="updateModel">the lot model</param>
        protected override Task InUpdate_BeforUpdateAsync<TUpdateModel>(Lot lot, TUpdateModel updateModel)
        {
            if (updateModel is LotPutModel model)
            {
                foreach (var lotProduct in lot.Products)
                {
                    var details = model.Products.FirstOrDefault(e => e.ProductId == lotProduct.ProductId);
                    if (details is null)
                        continue;

                    lotProduct.Quantity = details.Quantity;
                }
            }

            return Task.CompletedTask;
        }
    }
}
