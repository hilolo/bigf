﻿namespace Axiobat
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json.Serialization;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// extension methods to work with the JObject type
    /// </summary>
    public static partial class JObjectExtension
    {
        /// <summary>
        /// get the JObject instant for the given object
        /// </summary>
        /// <param name="obj">the object instant</param>
        /// <returns>the string value of the object</returns>
        public static JObject ToJObject<TEntity>(this TEntity obj)
            => JObject.FromObject(obj, new JsonSerializer
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
            });

        /// <summary>
        /// deserialize the given object into the given type
        /// </summary>
        /// <typeparam name="TDocument">the type of the document</typeparam>
        /// <param name="jObject">the JObject instant</param>
        /// <returns>the document</returns>
        public static TDocument As<TDocument>(this JObject jObject)
            => jObject.ToObject<TDocument>();

        /// <summary>
        /// check if the given object is empty
        /// </summary>
        /// <param name="jObject">the jObject instant</param>
        /// <returns>true if empty, false if not</returns>
        public static bool IsEmpty(this JObject jObject)
            => jObject is null || jObject.Count <= 0;

        /// <summary>
        /// get the id prop value from the JObject instant
        /// </summary>
        /// <param name="jObject">the JObject instant</param>
        /// <returns>the docType value</returns>
        public static string Id(this JObject jObject)
        {
            var IdProp = jObject["_id"];

            if (IdProp is null)
            {
                IdProp = jObject["id"];

                if (IdProp is null)
                {
                    IdProp = jObject["Id"];

                    if (IdProp is null)
                        return null;
                }
            }

            return IdProp.Value<string>();
        }

        /// <summary>
        /// get the id prop value from the JObject instant
        /// </summary>
        /// <param name="jObject">the JObject instant</param>
        /// <returns>the docType value</returns>
        public static TKey Id<TKey>(this JObject jObject)
        {
            var IdProp = jObject["_id"];

            if (IdProp is null)
            {
                IdProp = jObject["id"];

                if (IdProp is null)
                {
                    IdProp = jObject["Id"];
                    if (IdProp is null) return default;
                }
            }

            return IdProp.Value<TKey>();
        }

        /// <summary>
        /// get the client id from the json object
        /// </summary>
        /// <param name="jObject">the json </param>
        /// <returns></returns>
        public static string ClientId(this JObject jObject)
        {
            return jObject["clientId"]?.Value<string>("clientId") ?? string.Empty;
        }
    }
}
