﻿namespace Axiobat.Domain.Constants
{
    /// <summary>
    /// this class holds the emails templates
    /// </summary>
    public static partial class EmailTemplates
    {
        /// <summary>
        /// the template for transaction Failed event
        /// </summary>
        public const string TransactionFailed = "transaction_failed_email";

        /// <summary>
        /// the rest password template
        /// </summary>
        public const string RestPassword = "rest_password";

        /// <summary>
        /// the invitation Email template
        /// </summary>
        public const string InviteUser = "invite_user";

        /// <summary>
        /// the company invite accountant Email template
        /// </summary>
        public const string CompanyInviteAccountant = "company_invite_accountant";

        /// <summary>
        /// the accountant invite accountant Email template
        /// </summary>
        public const string AccountantInviteClient = "accountant_invite_client";

        /// <summary>
        /// the template for confirmation email
        /// </summary>
        public const string ConfirmEmail = "confirm_email";

        /// <summary>
        /// the template used in the card authentication failed event
        /// </summary>
        public const string CardAuthenticationFailed = "card_authentication_failed";

        /// <summary>
        /// the template for trial end
        /// </summary>
        public const string TrialEnd = "trial_end";

        /// <summary>
        /// the template for Grace Period Ended
        /// </summary>
        public const string GracePeriodEnded = "grace_period_ended";
    }

    /// <summary>
    /// partial part for <see cref="EmailTemplates"/>
    /// </summary>
    public static partial class EmailTemplates
    {

    }
}
