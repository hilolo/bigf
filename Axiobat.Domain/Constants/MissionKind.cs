﻿namespace Axiobat
{
    using System.Linq;

    /// <summary>
    /// the mission Type
    /// </summary>
    public static class MissionKind
    {
        /// <summary>
        /// list of all supported missions
        /// </summary>
        public static string[] ALL = new[] { TechnicianTask, Appointment, Call };

        /// <summary>
        /// a task assigned to a technician
        /// </summary>
        public const string TechnicianTask = "technician_task";

        /// <summary>
        /// an appointment with a customer
        /// </summary>
        public const string Appointment = "appointment";

        /// <summary>
        /// a call with a customer
        /// </summary>
        public const string Call = "call";

        /// <summary>
        /// check if the given kind is valid
        /// </summary>
        /// <param name="kind">the kind to be validated</param>
        /// <returns>true if valid false if not</returns>
        public static bool IsValid(string kind)
        {
            return ALL.Contains(kind);
        }
    }
}
