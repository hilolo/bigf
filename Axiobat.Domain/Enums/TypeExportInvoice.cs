﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of the invoice
    /// </summary>
    public enum TypeExportInvoice
    {
        /// <summary>
        /// the invoice don't have any type
        /// </summary>
        releve = 1,

        /// <summary>
        /// the invoice it a situation
        /// </summary>
        relance = 2,

    }
}
