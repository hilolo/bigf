﻿namespace Axiobat.Domain.Interfaces
{
    /// <summary>
    /// mark an entity as Referenceable, this meas that the entity will have a unique reference that is auto generated for it
    /// </summary>
    public interface IReferenceable<TEntity>
    {
        /// <summary>
        /// the reference, should be unique
        /// </summary>
        string Reference { get; set; }

        /// <summary>
        /// a function that hold the logic to check if we can increment the reference of the entity when creating a new one
        /// </summary>
        /// <returns>true if we can, false if not</returns>
        bool CanIncrementOnCreate();

        /// <summary>
        /// a function that hold the logic to check if we can increment the reference of the entity when updating a new one
        /// </summary>
        /// <param name="oldState">the old state of the entity before the update</param>
        /// <returns>true if we can, false if not</returns>
        bool CanIncrementOnUpdate(TEntity oldState);
    }
}
