﻿namespace Axiobat.Domain.Entities
{
    using Domain.Enums;
    using Domain.Interfaces;
    using System.Collections.Generic;

    /// <summary>
    /// the base class for all "product" related entity, including <see cref="Product"/> and <see cref="Lot"/>
    /// </summary>
    public abstract partial class ProductsBase
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.Undefined;

        /// <summary>
        /// the Designation
        /// </summary>
        public string Designation { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// mark an entity as deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// the changes history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="BibliothequeBase{TEntity}"/>
    /// </summary>
    /// <typeparam name="TEntity">the type of the entity</typeparam>
    public partial class ProductsBase : Entity<string>, IRecordable, IDeletable
    {
        /// <summary>
        /// create an instant of <see cref="ProductsBase"/>
        /// </summary>
        protected ProductsBase() : base()
        {
            Id = GenerateId();
            ChangesHistory = new List<ChangesHistory>();
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Name: {Designation}";

        /// <summary>
        /// generate an id for the document
        /// </summary>
        /// <returns>the new generated id</returns>
        public string GenerateId() => EntityId.Generate(DocumentType);
    }
}
