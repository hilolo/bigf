﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using System;

    /// <summary>
    /// this class defines a unit
    /// </summary>
    public partial class Unit : Entity<string>
    {
        /// <summary>
        /// the id of the <see cref="Tax"/> entity
        /// </summary>
        public override string Id
        {
            get => _id;
            set => _id = value.IsValid() ? value : Generator.GenerateRandomId();
        }

        /// <summary>
        /// the acronym of the unite, ex: m² , mm, cm, ...
        /// </summary>
        public string Acronym { get; set; }

        /// <summary>
        /// the full name of the unit, ex: 'centimeter', 'meter'
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Unit"/>
    /// </summary>
    public partial class Unit
    {
        private string _id;

        /// <summary>
        /// construct a new instant of <see cref="Unit"/>
        /// </summary>
        public Unit()
        {
        }

        /// <summary>
        /// construct a new instant of <see cref="Unit"/>
        /// </summary>
        /// <param name="acronym">the acronym of the unite, ex: m² , mm, cm, ...</param>
        /// <param name="name">the full name of the unit, ex: 'centimeter', 'meter'</param>
        public Unit(string acronym, string name) : this()
        {
            Acronym = acronym;
            Name = name;
        }

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Acronym} {Name}";

        /// <summary>
        /// check if the given object equals to the current instant
        /// </summary>
        /// <param name="obj">the object to check</param>
        /// <returns>true if equal, false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Unit)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return base.Equals(obj as Unit);
        }

        /// <summary>
        /// check if the given object instant equals to this instant
        /// </summary>
        /// <param name="other">the <see cref="Unit"/> to check</param>
        /// <returns>true if equals, false if not</returns>
        public bool Equals(Unit other)
            => other is null || !other.Acronym.Equals(Acronym, StringComparison.OrdinalIgnoreCase) ||
                !other.Name.Equals(Name, StringComparison.OrdinalIgnoreCase) ? false : true;

        /// <summary>
        /// get the hashCode of the object
        /// </summary>
        /// <returns>the hashCode</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ (Name?.GetHashCode() ?? 0);
                return (hashCode * 397) ^ (Acronym?.GetHashCode() ?? 0);
            }
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString() => $"Name: {Name}, Acronym: {Acronym}";
    }
}
