﻿namespace Axiobat.Domain.Entities
{
    using Domain.Exceptions;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class defines the chart of accounts
    /// </summary>
    public partial class ChartOfAccounts
    {
        /// <summary>
        /// the general plans account id
        /// </summary>
        public const string General = "1";

        /// <summary>
        /// the Profits plans account id
        /// </summary>
        public const string Profits = "3";

        /// <summary>
        /// the expense plans account id
        /// </summary>
        public const string Expenses = "2";

        /// <summary>
        /// the taxes plans account id
        /// </summary>
        public const string Taxes = "4";

        /// <summary>
        /// the assets Chart Of Accounts
        /// </summary>
        public const string Assets = "5";

        /// <summary>
        /// list of charts
        /// </summary>
        public IEnumerable<ChartAccountItem> Charts { get; }
    }

    public partial class ChartOfAccounts
    {
        private IDictionary<string, ChartAccountItem> _chartOfAccounts;

        public ChartOfAccounts(IEnumerable<ChartAccountItem> charts)
        {
            Charts = charts ?? throw new ArgumentNullException(nameof(charts));
            _chartOfAccounts = charts.ToDictionary(e => e.Id);
        }

        public static ChartOfAccounts Create(IEnumerable<ChartAccountItem> charts)
        {
            return new ChartOfAccounts(charts);
        }

        /// <summary>
        /// get the chart of account Item with the given id
        /// </summary>
        /// <param name="chartId">the id of the chart of account</param>
        /// <returns>the <see cref="ChartAccountItem"/> instant</returns>
        public ChartAccountItem this[string chartId] => _chartOfAccounts[chartId];

        /// <summary>
        /// get the plan with the given id
        /// </summary>
        /// <param name="planId">the id of the plan</param>
        /// <returns>the <see cref="AccountingPlan"/> instant</returns>
        public ChartAccountItem this[string chartId, string chartSubItemId] => this[chartId][chartSubItemId];

        /// <summary>
        /// get the tax code for the given VAT value
        /// </summary>
        /// <param name="VATValue">the VAT value to get the code for it</param>
        /// <returns>the code value</returns>
        public string GetTaxCode(float VATValue)
        {
            var taxesPlan = this[Taxes];
            if (taxesPlan is null)
                throw new AxiobatException($"there is no Taxes plans with id: {Taxes} in the company params");

            return taxesPlan.SubCategories
                .FirstOrDefault(e => e.VATValue == VATValue)?.Code ??
                 taxesPlan[ChartAccountItem.DefaultTax].Code;
        }

        /// <summary>
        /// get the code of the Deductible VAT
        /// </summary>
        /// <returns>code</returns>
        public string GetTaxDeductibleCode()
        {
            var taxesPlan = this[Taxes];
            if (taxesPlan is null)
                throw new AxiobatException($"there is no Taxes plans with id: {Taxes} in the company params");

            return taxesPlan[ChartAccountItem.DeductibleTax].Code;
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"[{Charts.Count()}] Plans";

        public static bool CanDelete(string id)
        {
            switch (id)
            {
                case ChartAccountItem.Bank :
                case ChartAccountItem.Cash :
                case ChartAccountItem.Clients :
                case ChartAccountItem.CreditNote :
                case ChartAccountItem.DeductibleTax :
                case ChartAccountItem.AssetsTax :
                case ChartAccountItem.Default :
                case ChartAccountItem.DefaultTax :
                case ChartAccountItem.InternalTransfers :
                case ChartAccountItem.Suppliers :
                case General :
                case Profits :
                case Expenses :
                case Taxes :
                case Assets:
                    return false;
                default:
                    return true;
            }
        }
    }
}
