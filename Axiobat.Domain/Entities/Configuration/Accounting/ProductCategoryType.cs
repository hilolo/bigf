﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System.Collections.Generic;

    /// <summary>
    /// this class is user to define products categories types
    /// </summary>
    public partial class ProductCategoryType
    {
        /// <summary>
        /// a label for the category type
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// a description for the product category type
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// type of the product category
        /// </summary>
        public CategoryType Type { get; set; }

        /// <summary>
        /// list of products associated with this category
        /// </summary>
        public ICollection<Product> Products { get; set; }
    }

    /// <summary>
    /// the type of the
    /// </summary>
    public partial class ProductCategoryType : Entity<string>, System.IEquatable<ProductCategoryType>
    {
        /// <summary>
        /// create an instant of <see cref="ProductCategoryType"/>
        /// </summary>
        public ProductCategoryType() : base()
        {
            Products = new HashSet<Product>();
        }

        /// <inheritdoc/>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Label}";

        /// <inheritdoc/>
        public override string ToString()
            => $"{Label}";

        /// <summary>
        /// check if the given object equals the c
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(ProductCategoryType other)
            => !(other is null) &&
                    other.Id == other.Id &&
                    other.Label == Label &&
                    other.Description == Description;

        /// <summary>
        /// check if the given object equals the c
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(ProductCategoryType)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as ProductCategoryType);
        }

        /// <summary>
        /// get the hash code value of the entity
        /// </summary>
        /// <returns>the has value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = -1880813789;
                hashCode = hashCode * -1521134295 + Type.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Id);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Label);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Description);
                return hashCode;
            }
        }

        public static bool operator ==(ProductCategoryType left, ProductCategoryType right)
            => EqualityComparer<ProductCategoryType>.Default.Equals(left, right);

        public static bool operator !=(ProductCategoryType left, ProductCategoryType right)
            => !(left == right);
    }
}
