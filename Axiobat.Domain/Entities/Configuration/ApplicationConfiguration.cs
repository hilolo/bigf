﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using Axiobat.Domain.Enums;
    using Domain.Constants;
    using System;

    /// <summary>
    /// this class represent the user configuration
    /// </summary>
    [DocType(DocumentType.Configuration)]
    public partial class ApplicationConfiguration
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocumentType => DocumentType.Configuration;

        /// <summary>
        /// the type of the setting, one of <see cref="ApplicationConfigurationType"/>
        /// </summary>
        public string ConfigurationType { get; set; }

        /// <summary>
        /// the value of the setting, Note that this is a JSON value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// the last time the model has been modified
        /// </summary>
        public DateTimeOffset LastModifiedOn { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="ApplicationConfiguration"/>
    /// </summary>
    public partial class ApplicationConfiguration
    {
        /// <summary>
        /// create an instant of <see cref="ApplicationConfiguration"/>
        /// </summary>
        private ApplicationConfiguration()
        {
            LastModifiedOn = DateTimeOffset.Now;
        }

        /// <summary>
        /// get the value as the given output type
        /// </summary>
        /// <typeparam name="TOut">the desired output type</typeparam>
        /// <returns>the output result</returns>
        public TOut GetValue<TOut>() => Value.FromJson<TOut>();

        /// <summary>
        /// return string value of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"type: {ConfigurationType}";
    }
}
