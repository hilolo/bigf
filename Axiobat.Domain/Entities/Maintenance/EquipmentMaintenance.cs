﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the necessary information that defines the equipment maintenance
    /// </summary>
    [DocType(DocumentType.EquipmentMaintenance)]
    public partial class EquipmentMaintenance
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.EquipmentMaintenance;

        /// <summary>
        /// the name of the Equipment
        /// </summary>
        public string EquipmentName { get; set; }

        /// <summary>
        /// list of the equipment operations
        /// </summary>
        public ICollection<EquipmentMaintenanceOperation> MaintenanceOperations { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="EquipmentMaintenance"/>
    /// </summary>
    public partial class EquipmentMaintenance : Entity<string>, IEquatable<EquipmentMaintenance>
    {
        /// <summary>
        /// create an instant of <see cref="EquipmentMaintenance"/>
        /// </summary>
        public EquipmentMaintenance()
        {
            Id = GenerateId();
            MaintenanceOperations = new HashSet<EquipmentMaintenanceOperation>();
        }

        /// <summary>
        /// generate an id for the document
        /// </summary>
        /// <returns>the new generated id</returns>
        public string GenerateId() => EntityId.Generate(DocumentType);

        /// <summary>
        /// build the search terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{EquipmentName}";

        /// <summary>
        /// check if the two instants are equals
        /// </summary>
        /// <param name="other">the other instant to check</param>
        /// <returns>true if equals false if not</returns>
        public override bool Equals(object other)
        {
            if (ReferenceEquals(this, other)) return true;
            return Equals(other as EquipmentMaintenance);
        }

        /// <summary>
        /// check if the two instants are equals
        /// </summary>
        /// <param name="other">the other instant to check</param>
        /// <returns>true if equals false if not</returns>
        public bool Equals(EquipmentMaintenance other)
            => other != null && EquipmentName == other.EquipmentName && Id == other.Id;

        /// <summary>
        /// get the hash code
        /// </summary>
        /// <returns>the hash code value</returns>
        public override int GetHashCode()
        {
            var hashCode = 993295730;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(EquipmentName);
            return hashCode;
        }

        /// <summary>
        /// build the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Equipment {EquipmentName} has {MaintenanceOperations} maintenance operations";

        public static bool operator ==(EquipmentMaintenance left, EquipmentMaintenance right) => EqualityComparer<EquipmentMaintenance>.Default.Equals(left, right);
        public static bool operator !=(EquipmentMaintenance left, EquipmentMaintenance right) => !(left == right);
    }
}
