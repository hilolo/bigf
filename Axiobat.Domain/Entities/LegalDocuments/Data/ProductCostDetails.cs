﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the product sales details
    /// </summary>
    public partial class ProductCostDetails
    {
        /// <summary>
        /// the total material cost
        /// </summary>
        public float TotalMaterialCost { get; set; }

        /// <summary>
        /// the total cost
        /// </summary>
        public float TotalCost { get; set; }

        /// <summary>
        /// the total Hours
        /// </summary>
        public int TotalHours { get; set; }
    }
}
