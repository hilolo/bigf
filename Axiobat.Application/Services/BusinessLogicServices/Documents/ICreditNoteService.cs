﻿namespace Axiobat.Application.Services.Documents
{
    using Models;
    using Domain.Entities;
    using System.Threading.Tasks;
    using FileService;
    using System.Collections.Generic;

    /// <summary>
    /// the service for <see cref="Expense"/>
    /// </summary>
    public interface ICreditNoteService : IDocumentService<CreditNote>, IDuplicableService, IMemoService, IEmailDocument<string>
    {
        /// <summary>
        /// generate a credit note to cancel an invoice
        /// </summary>
        /// <param name="invoice">the invoice to be canceled</param>
        /// <returns>the generated credit note</returns>
        Task<Result<CreditNoteModel>> GenerateForInvoiceCancelationAsync(Invoice invoice);

        /// <summary>
        /// update the status of the credit note with the given id
        /// </summary>
        /// <param name="creditNoteId">the id of the credit note</param>
        /// <param name="status">the status to be assigned to it</param>
        Task UpdateStatusAsync(string creditNoteId, string status);

        /// <summary>
        /// create a credit note for the payment with the given invoice
        /// </summary>
        /// <param name="invoice">the invoice to generate the credit note for it</param>
        /// <returns>the created credit note with the desired output</returns>
        Task<Result<TOut>> CreateForPaymentAsync<TOut>(Invoice invoice, double totalPayment);
    }
}
