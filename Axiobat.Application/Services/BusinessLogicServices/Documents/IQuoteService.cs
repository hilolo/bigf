﻿namespace Axiobat.Application.Services.Documents
{
    using FileService;
    using Domain.Entities;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="Quote"/>
    /// </summary>
    public interface IQuoteService : IDocumentService<Quote>, IEmailDocument<string>, IDuplicableService, IMemoService
    {
        /// <summary>
        /// update the status of the quote
        /// </summary>
        /// <param name="quoteId">the id of the quote to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        Task<Result> UpdateStatusAsync(string quoteId, DocumentUpdateStatusModel model);

        /// <summary>
        /// generate Suppliers orders from the given quote
        /// </summary>
        /// <param name="quoteId">the id of the quote</param>
        /// <returns>the operation result</returns>
        Task<ListResult<MinimalDocumentModel>> GenerateSupplierOrderAsync(string quoteId);

        /// <summary>
        /// generate an invoice from the quote with given id
        /// </summary>
        /// <param name="quoteId">the id of the quote</param>
        /// <returns>the operation result</returns>
        Task<Result<InvoiceModel>> GenerateInvoiceFromQuoteAsync(string quoteId);

        /// <summary>
        /// update the Quote situation list, id the situation exist it will be updated, if not it will be added
        /// </summary>
        /// <param name="quoteId">the id of the quote</param>
        /// <param name="quoteSituations">the quoteSituations</param>
        Task UpdateListSituationsAsync(string quoteId, QuoteSituations quoteSituations);

        /// <summary>
        /// remove the invoice form the list of situation
        /// </summary>
        /// <param name="quoteId">the id of the quote to be updated</param>
        /// <param name="invoiceId">the id of the invoice to be deleted</param>
        Task DeleteInvoiceFromListSituationsAsync(string quoteId, string invoiceId);

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        Task<Result> UpdateSignatureAsync(string quoteId, DocumentUpdateSignatureModel model);

        /// <summary>
        /// after invoice has been Deleted/Canceled this method will remove quote related data
        /// and update it status
        /// </summary>
        /// <param name="quoteId">the id of the quote to be updated</param>
        /// <param name="invoiceId">the id of the deleted/canceled invoice</param>
        Task QuoteInvoiceDeletedAsync(string quoteId, string invoiceId);

        /// <summary>
        /// after invoice has been Canceled this method will 
        /// and update it status
        /// </summary>
        /// <param name="quoteId">the id of the quote to be updated</param>
        /// <param name="invoiceId">the id of the deleted/canceled invoice</param>
        Task QuoteInvoiceChangeStatutCancelAsync(string quoteId, string invoiceId);
    }
}
