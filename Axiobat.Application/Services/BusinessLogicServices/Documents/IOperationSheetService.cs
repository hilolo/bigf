﻿namespace Axiobat.Application.Services.Documents
{
    using Configuration;
    using Domain.Entities;
    using FileService;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="OperationSheet"/>
    /// </summary>
    public interface IOperationSheetService : IDataService<OperationSheet, string>, IReferenceService, IMemoService, ISynchronize
    {
        /// <summary>
        /// send the operationSheet with the given id with an email using the given options
        /// </summary>
        /// <param name="operationSheetId">the id of the document</param>
        /// <param name="emailOptions">the email options</param>
        /// <returns>the operation result</returns>
        Task<Result> SendAsync(string operationSheetId, SendEmailOptions emailOptions);

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        Task<Result> UpdateStatusAsync(string operationSheetId, DocumentUpdateStatusModel model);

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        Task<Result> UpdateModeStatusAsync(string operationSheetId, ModeStatut model);


        /// <summary>
        /// retrieve the list of operation sheet of the given client
        /// </summary>
        /// <typeparam name="TOut">the type of the desired output</typeparam>
        /// <param name="clientId">the id of the client</param>
        /// <returns>list of operation sheets</returns>
        Task<ListResult<TOut>> GetClientOperationSheetsAsync<TOut>(string clientId);

        /// <summary>
        /// retrieve the operation sheet with the given reference
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="operationSheetReference">the operation sheet reference</param>
        /// <returns>the operation sheet</returns>
        Task<Result<TOut>> GetByReferenceAsync<TOut>(string operationSheetReference);
        Task updateForCalendarIdAsync();

        /// <summary>
        /// duplicate the operation sheet
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="operationSheetId">the operation sheet Id</param>
        /// <returns>the out put model</returns>
        Task<Result<TOut>> DuplicateAsync<TOut>(string operationSheetId);

        /// <summary>
        /// bill the operation sheets with the given ids, this function will retrieve the operation sheets with the given ids
        /// and update their status to <see cref="Domain.Constants.OperationSheetStatus.Billed"/>
        /// </summary>
        /// <param name="invoiceId">the id of the invoice</param>
        /// <param name="operationSheetId">the id of the operation sheet</param>
        Task BillOperationSheetsAsync(string invoiceId, params string[] operationSheetId);

        /// <summary>
        /// update the status of all operation sheets associated with the given invoice
        /// </summary>
        /// <param name="invoiceId">the id of the invoice</param>
        /// <param name="model">the status update model</param>
        Task UpdateInvoiceOperationSheetStatusAsync(string invoiceId, DocumentUpdateStatusModel model);

        Task<Result> UpdateSignatureAsync(string operationSheetId, DocumentUpdateSignatureModel model);
    }
}
