﻿namespace Axiobat.Application.Services.Configuration
{
    using Application.Models;
    using Axiobat.Domain.Entities;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for managing publishing files
    /// </summary>
    public interface IPublishingService
    {
        /// <summary>
        /// extracting the tags from the given document steam
        /// </summary>
        /// <param name="stream">the file stream</param>
        /// <returns>the list of tags</returns>
        string[] ExtractTagsFromDocument(Stream stream);

        /// <summary>
        /// save the given document (steam) using the given name
        /// </summary>
        /// <param name="stream">the document stream to save the </param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        Task<string> SaveContractAsync(Stream stream, string fileName);

        /// <summary>
        /// create a new publish contract recored
        /// </summary>
        /// <param name="model">the model to be used for creating a new contract</param>
        /// <returns>the operation result</returns>
        Task<Result<PublishingContractModel>> CreateAsync(PublishingContractCreateModel model);

        /// <summary>
        /// update the publishing contract with the given id using the given model
        /// </summary>
        /// <param name="contractId">the id of the contract</param>
        /// <param name="model">the update model</param>
        /// <returns>the updated version</returns>
        Task<Result<PublishingContractModel>> UpdateAsync(int contractId, PublishingContractUpdateModel model);

        /// <summary>
        /// delete the publishing contract with the given id
        /// </summary>
        /// <param name="contractId">the id of the publishing contract to be deleted</param>
        /// <returns>the operation result</returns>
        Task<Result> DeleteAsync(int contractId);

        /// <summary>
        /// get the list of publishing contracts as a paged result
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>the paged result</returns>
        Task<PagedResult<PublishingContractModel>> GetAsPagedResultAsync(FilterOptions filterModel);

        /// <summary>
        /// validate if the given file name is support file
        /// </summary>
        /// <param name="fileName">the name of the file</param>
        void ValidateFileName(string fileName);

        /// <summary>
        /// download contract with the given id
        /// </summary>
        /// <param name="contractId">the id of the contract</param>
        /// <returns>the download details</returns>
        Task<PublishingContractDownloadModel> DownloadContractAsync(int contractId);

        Task<PublishingContractDownloadModel> DownloadContractAsync(PublishingContractMinimal publishingContract);
    }
}
