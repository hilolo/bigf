﻿namespace Axiobat.Application.Services
{
    using Domain.Enums;
    using Enums;
    using Models;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this service is responsible for all date related operation
    /// </summary>
    public interface IDateService
    {
        /// <summary>
        /// get the total working hours between the given starting date and ending date, taking in consideration the starting working hours and ending hours
        /// </summary>
        /// <param name="startingDate">the starting date</param>
        /// <param name="endingDate">the ending date</param>
        /// <param name="startingHours">the starting hour</param>
        /// <param name="endingHours">the ending hour</param>
        /// <returns>the total working hours</returns>
        double GetTotalWorkingHours(DateTime startingDate, DateTime endingDate, string startingHours, string endingHours);

        /// <summary>
        /// use this method to set the date range based on the given date filter
        /// </summary>
        /// <param name="dateRangeFilter">the date range filter</param>
        void SetDateRange(DateRangeFilterOptions dateRangeFilter);

        /// <summary>
        /// use this method to set the date range based on the given date filter, and than extend the period using the given extend options
        /// </summary>
        /// <param name="dateRangeFilter">the date range filter</param>
        /// <param name="extendOptions">the period extend options</param>
        /// <param name="extendDirection">the extend direction</param>
        /// <param name="value">the extend value</param>
        void SetDateRange(DateRangeFilterOptions dateRangeFilter, PeriodType extendOptions, DateExtendDirection extendDirection, sbyte value);

        /// <summary>
        /// use this method to set the date range based on the given date filter
        /// </summary>
        /// <param name="dateRangeFilter">the date range filter</param>
        /// <param name="accountingPeriod">the accounting period</param>
        void SetDateRange(DateRangeFilterOptions dateRangeFilter, AccountingPeriodModel accountingPeriod);

        /// <summary>
        /// use this method to set the date range based on the given date filter
        /// </summary>
        /// <param name="dateRangeFilter">the date range filter</param>
        /// <param name="accountingPeriod">the accounting period</param>
        /// <param name="extendOptions">the period extend options</param>
        /// <param name="extendDirection">the extend direction</param>
        /// <param name="value">the extend value</param>
        void SetDateRange(DateRangeFilterOptions dateRangeFilter, AccountingPeriodModel accountingPeriod, PeriodType extendOptions, DateExtendDirection extendDirection, sbyte value);

        /// <summary>
        /// get the list of dates between the given start and end date, this will return the 1et of each month in every year between the given two dates
        /// </summary>
        /// <param name="startDate">the start date</param>
        /// <param name="endDate">the end date</param>
        /// <returns>the list of the dates</returns>
        IEnumerable<DateTime> GetDates(DateTime startDate, DateTime endDate);
    }
}