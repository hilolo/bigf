﻿namespace Axiobat.Application.Services.Configuration
{
    using Data;
    using Domain.Entities;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the country service
    /// </summary>
    public interface ICountryService
    {
        /// <summary>
        /// get paged result of the countries
        /// </summary>
        /// <param name="filter">the filter option</param>
        /// <returns>list of countries</returns>
        Task<PagedResult<TOut>> GetAsPagedResultAsync<TOut, IFilter>(IFilter filterOption) where IFilter : IFilterOptions;

        /// <summary>
        /// get the country by code
        /// </summary>
        /// <param name="countryCode">the code of the country</param>
        /// <returns>the country</returns>
        Task<Result<Country>> GetByCodeAsync(string countryCode);

        /// <summary>
        /// the name of the country
        /// </summary>
        /// <param name="countryName">the name of the country</param>
        /// <returns>the country</returns>
        Task<Result<Country>> GetByNameAsync(string countryName);
    }
}