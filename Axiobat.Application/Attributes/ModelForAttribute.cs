﻿namespace Axiobat.Application
{
    /// <summary>
    /// this attribute is for defining the entity that the model is build for it
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    [System.AttributeUsage(System.AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class ModelForAttribute : System.Attribute
    {
        /// <summary>
        /// a constructor with the property name
        /// </summary>
        /// <param name="propretyName">the name of the property to be used</param>
        public ModelForAttribute(System.Type entityType)
        {
            if (entityType is null)
                throw new System.ArgumentException("you must supply a Type or remove the attribute", nameof(entityType));

            EntityType = entityType;
        }

        /// <summary>
        /// name of the property
        /// </summary>
        public System.Type EntityType { get; }
    }
}
