﻿namespace Axiobat.Application.Exceptions
{
    using System;

    /// <summary>
    /// the validation exception associated with the unauthorized cases
    /// </summary>
    [Serializable]
    public class InvalidLoginCredentialsException : ValidationException
    {
        private static readonly string _message = "the given login credentials are not valid";

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error
        /// message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public InvalidLoginCredentialsException() : base(_message, Axiobat.MessageCode.InvalidLoginCredentials)
        {
        }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with a specified error
        /// message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public InvalidLoginCredentialsException(string message) : base(message, Axiobat.MessageCode.InvalidLoginCredentials)
        {
        }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with serialized data.
        /// </summary>
        /// <param name="serializationInfo">The System.Runtime.Serialization.SerializationInfo that holds the serialized object data about the exception being thrown.</param>
        /// <param name="streamingContext">The System.Runtime.Serialization.StreamingContext that contains contextual information about the source or destination.</param>
        protected InvalidLoginCredentialsException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext)
            : base(_message, Axiobat.MessageCode.NotFound)
        {
            throw new NotImplementedException();
        }
    }
}
