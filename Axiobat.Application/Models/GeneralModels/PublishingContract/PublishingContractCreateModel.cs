﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// a class describe publishing contract requirement
    /// </summary>
    public class PublishingContractCreateModel
    {
        /// <summary>
        /// the name of contract
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// the tags of contract
        /// </summary>
        public IDictionary<string, string> Tags { get; set; }

        /// <summary>
        /// the name of file
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// the original file name
        /// </summary>
        public string OrginalFileName { get; set; }
    }
}
