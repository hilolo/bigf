﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Enums;

    /// <summary>
    /// the filter options for maintenance OperationSheet
    /// </summary>
    public partial class MaintenanceOperationSheetFilterOptions : OperationSheetFilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="MaintenanceOperationSheetFilterOptions"/>
        /// </summary>
        public MaintenanceOperationSheetFilterOptions() : base()
        {

        }

        /// <summary>
        /// get the Maintenance Operation Sheet with quote request
        /// </summary>
        public bool? HasQuoteRequest { get; set; }

        /// <summary>
        /// the type of the Maintenance Operation Sheet
        /// </summary>
        public MaintenanceOperationSheetType? Type { get; set; }
    }
}
