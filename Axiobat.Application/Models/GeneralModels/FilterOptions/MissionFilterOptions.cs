﻿namespace Axiobat.Application.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the filter option for <see cref="Mission"/>
    /// </summary>
    public partial class MissionFilterOptions : DateRangeFilterOptions
    {
        public MissionFilterOptions()
        {
            Types = new HashSet<string>();
            Kinds = new HashSet<string>();
            Status = new HashSet<string>();
            TechnicianIds = new HashSet<Guid>();
        }

        /// <summary>
        /// the id of the Technician
        /// </summary>
        public ICollection<Guid> TechnicianIds { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// list of status to check
        /// </summary>
        public ICollection<string> Status { get; set; }

        /// <summary>
        /// list of kinds to retrieve
        /// </summary>
        public ICollection<string> Kinds { get; set; }

        /// <summary>
        /// list of types to retrieve
        /// </summary>
        public ICollection<string> Types { get; set; }
    }
}
