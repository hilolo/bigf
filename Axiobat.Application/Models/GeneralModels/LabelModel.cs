﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Label"/>
    /// </summary>
    [ModelFor(typeof(Label))]
    public partial class LabelModel
    {
        /// <summary>
        /// the id of the label
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the value of the label
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="LabelModel"/>
    /// </summary>
    public partial class LabelModel : IModel<Label, string>, IUpdateModel<Label>, IEquatable<LabelModel>
    {
        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity</param>
        public void Update(Label entity)
        {
            entity.Value = Value;
        }

        /// <summary>
        /// build the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"id: {Id}, value: {Value}";

        public override bool Equals(object obj)
        {
            return Equals(obj as LabelModel);
        }

        public bool Equals(LabelModel other)
        {
            return other != null &&
                   Id == other.Id &&
                   Value == other.Value;
        }

        public override int GetHashCode()
        {
            var hashCode = 1325046378;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Value);
            return hashCode;
        }

        public static bool operator ==(LabelModel left, LabelModel right)
            => EqualityComparer<LabelModel>.Default.Equals(left, right);

        public static bool operator !=(LabelModel left, LabelModel right)
            => !(left == right);
    }
}
