﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the model for <see cref="Unit"/>
    /// </summary>
    [ModelFor(typeof(Unit))]
    public partial class UnitModel
    {
        /// <summary>
        /// the id of the unit
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the acronym of the unite, ex: m² , mm, cm, ...
        /// </summary>
        public string Acronym { get; set; }

        /// <summary>
        /// the full name of the unit, ex: 'centimeter', 'meter'
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="UnitModel"/>
    /// </summary>
    public partial class UnitModel : IModel<Unit, string>, IUpdateModel<Unit>
    {
        /// <summary>
        /// update the entity for the current model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public void Update(Unit entity)
        {
            entity.Acronym = Acronym;
            entity.Name = Name;
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString() => $"Name: {Name}, Acronym: {Acronym}";
    }
}
