﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;

    /// <summary>
    /// the model for <see cref="FinancialAccount"/>
    /// </summary>
    [ModelFor(typeof(FinancialAccount))]
    public partial class FinancialAccountModel : IModel<FinancialAccount, int>, IUpdateModel<FinancialAccount>
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// type of the account
        /// </summary>
        public FinancialAccountsType Type { get; set; }

        /// <summary>
        /// the account label
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// the accounting code associated with the financial account
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// is this account the default one
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// update the entity from the given model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public void Update(FinancialAccount entity)
        {
            entity.Type = Type;
            entity.Label = Label;
            entity.Code = Code;
        }
    }
}
