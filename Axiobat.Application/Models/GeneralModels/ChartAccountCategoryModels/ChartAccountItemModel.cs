﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// a sub item model for <see cref="ChartAccountItem"/>
    /// </summary>
    [ModelFor(typeof(ChartAccountItem))]
    public partial class ChartAccountSubItemModel : IModel<ChartAccountItem>
    {
        /// <summary>
        /// the id of the category
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the type of the category
        /// </summary>
        public CategoryType CategoryType { get; set; }

        /// <summary>
        /// the label of the category
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// the accounting code associated with this category
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// a description of the category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the value of the tax, only check for this value when the <see cref="Type"/> is <see cref="ChartAccountType.VAT"/>
        /// </summary>
        public float? VATValue { get; set; }

        /// <summary>
        /// the id of the parent category
        /// </summary>
        public string ParentId { get; set; }
    }

    /// <summary>
    /// model for <see cref="ChartAccountItem"/>
    /// </summary>
    [ModelFor(typeof(ChartAccountItem))]
    public partial class ChartAccountItemModel
    {
        /// <summary>
        /// the id of the category
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// type of the chart category
        /// </summary>
        public ChartAccountType Type { get; set; }

        /// <summary>
        /// the label of the category
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// the accounting code associated with this category
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// a description of the category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the list of sub categories owned by this category
        /// </summary>
        public ICollection<ChartAccountSubItemModel> SubCategories { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="ChartAccountItemModel"/>
    /// </summary>
    public partial class ChartAccountItemModel : IModel<ChartAccountItem, string>
    {
        /// <summary>
        /// create an instant of <see cref="ChartAccountItemModel"/>
        /// </summary>
        public ChartAccountItemModel()
        {
            SubCategories = new HashSet<ChartAccountSubItemModel>();
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"id: {Id}, label: {Label}, type: {Type}";
    }
}
