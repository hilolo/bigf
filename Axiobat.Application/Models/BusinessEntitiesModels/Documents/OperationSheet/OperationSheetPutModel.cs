﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// model used for updating and creating an operation sheet
    /// </summary>
    [ModelFor(typeof(OperationSheet))]
    public partial class OperationSheetPutModel
    {
        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the status of the operation sheet, one of <see cref="Domain.Constants.OperationSheetStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the order details associated with this operation sheet
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the Report associated with the operation sheet
        /// </summary>
        public string Report { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the count of the Visits the technician has made to the client
        /// </summary>
        public int VisitsCount { get; set; }

        /// <summary>
        /// the total Basket consumption the technician has made
        /// </summary>
        public int TotalBasketConsumption { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        public string QuoteId { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        [PropertyName("Client")]
        [PropertyValue("Client.FullName")]
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// technicians ids list to affect to this operation sheet
        /// </summary>
        public ICollection<Guid> Technicians { get; set; }


        /// <summary>
        /// ModeStatut  to affect to this operation sheet
        /// </summary>
        public ICollection<ModeStatut> ModeStatut { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="OperationSheetPutModel"/>
    /// </summary>
    public partial class OperationSheetPutModel : IUpdateModel<OperationSheet>
    {
        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public void Update(OperationSheet entity)
        {
            entity.Status = Status;
            entity.OrderDetails = OrderDetails;
            entity.StartDate = StartDate;
            entity.EndDate = EndDate;
            entity.Report = Report;
            entity.Purpose = Purpose;
            entity.VisitsCount = VisitsCount;
            entity.TotalBasketConsumption = TotalBasketConsumption;
            entity.WorkshopId = WorkshopId;
            entity.QuoteId = QuoteId;
            entity.Client = Client;
            entity.ClientId = ClientId;
            entity.AddressIntervention = AddressIntervention;
            entity.Reference = Reference;
        }
    }
}
