﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// the list model for <see cref="SupplierOrder"/>
    /// </summary>
    [ModelFor(typeof(SupplierOrder))]
    public partial class SupplierOrderListModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the Supplier Order
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the Supplier associated with this Supplier Order
        /// </summary>
        public string Supplier { get; set; }

        /// <summary>
        /// the date the Supplier Order should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Supplier Order creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the total HT of the Supplier Order
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the total TTC of the Supplier Order
        /// </summary>
        public float TotalTTC { get; set; }
    }
}
