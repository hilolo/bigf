﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the model for <see cref="Document"/> for updating the status
    /// </summary>
    [ModelFor(typeof(Document))]
    public partial class DocumentUpdateSignatureModel
    {
        /// <summary>
        /// the Signature of the client
        /// </summary>
        public Signature ClientSignature { get; set; }

        /// <summary>
        /// the technicien signature
        /// </summary>
        public Signature TechnicianSignature { get; set; }
    }
}
