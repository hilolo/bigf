﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System;

    /// <summary>
    /// the list model for <see cref="Invoice"/>
    /// </summary>
    [ModelFor(typeof(Invoice))]
    public partial class InvoiceListModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the quote
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the type of invoice
        /// </summary>
        public InvoiceType TypeInvoice { get; set; }

        /// <summary>
        /// the date the invoice should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// invoice creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the workshop associated with this invoice
        /// </summary>
        public string Workshop { get; set; }

        /// <summary>
        /// the client associated with this invoice
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// the purpose of the document
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the total HT of the invoice
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the total TTC of the invoice
        /// </summary>
        public float TotalTTC { get; set; }

        /// <summary>
        /// the rest to pay of the invoice
        /// </summary>
        public float RestToPay { get; set; }

        /// <summary>
        /// the reference of the contract associated with this invoice
        /// </summary>
        public string ContractReference { get; set; }

        /// <summary>
        /// /// the reference of the Operation Sheet Maintenance associated with this invoice
        /// </summary>
        public string OperationSheetMaintenanceReference { get; set; }

        /// <summary>
        /// a boolean check if you can delete this invoice
        /// </summary>
        public bool CanDelete { get; set; }

        /// <summary>
        /// a boolean check if you can delete this invoice
        /// </summary>
        public bool CanUpdate { get; set; }

        /// <summary>
        /// a boolean check if you can cancel this invoice
        /// </summary>
        public bool CanCancel { get; set; }

    }
}
