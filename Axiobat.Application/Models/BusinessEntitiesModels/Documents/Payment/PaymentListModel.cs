﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;
    using System;

    /// <summary>
    /// the list model for <see cref="Payment"/>
    /// </summary>
    [ModelFor(typeof(Payment))]
    public partial class PaymentListModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// a description associated wit the payment
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the type of this payment
        /// </summary>
        public PaymentType Type { get; set; }

        /// <summary>
        /// the type of the payment operation
        /// </summary>
        public PaymentOperation Operation { get; set; }

        /// <summary>
        /// the date of the payment
        /// </summary>
        public DateTime DatePayment { get; set; }

        /// <summary>
        /// whether this invoice is closed by the accounting period or not
        /// </summary>
        public bool Closed { get; set; }

        /// <summary>
        /// the payment amount
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// the account associated with this payment
        /// </summary>
        public string Account { get; set; }
    }
}
