﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the product supplier information
    /// </summary>
    [ModelFor(typeof(ProductSupplier))]
    public class ProductSupplierInfo
    {
        /// <summary>
        /// the id of the supplier
        /// </summary>
        public string SupplierId { get; set; }

        /// <summary>
        /// the supplier related to this pricing details
        /// </summary>
        public SupplierModel Supplier { get; set; }

        /// <summary>
        /// the price of the product by supplier
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// if this product is the default one for this supplier
        /// </summary>
        public bool IsDefault { get; set; }
    }
}
