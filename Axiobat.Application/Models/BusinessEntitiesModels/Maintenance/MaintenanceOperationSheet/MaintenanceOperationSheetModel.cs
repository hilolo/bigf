﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="MaintenanceOperationSheet"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceOperationSheet))]
    public partial class MaintenanceOperationSheetModel : IModel<MaintenanceOperationSheet>
    {
        public MaintenanceOperationSheetModel()
        {
            AssociatedDocuments = new HashSet<AssociatedDocument>();
        }

        /// <summary>
        /// the id of the model
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the Maintenance Operation Sheet, one of the values of <see cref="Constants.MaintenanceOperationSheetStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the Report associated with the operation sheet
        /// </summary>
        public string Report { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the type of the Maintenance OperationSheet
        /// </summary>
        public MaintenanceOperationSheetType Type { get; set; }

        /// <summary>
        /// the count of the Visits the technician has made to the client
        /// </summary>
        public int VisitsCount { get; set; }

        /// <summary>
        /// the total Basket consumption the technician has made
        /// </summary>
        public int TotalBasketConsumption { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the request for creating a quote
        /// </summary>
        public string QuoteRequest { get; set; }

        /// <summary>
        /// the client associated with this operation sheet
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the Technician associated with this MaintenanceOperationSheet
        /// </summary>
        public UserMinimalModel Technician { get; set; }

        /// <summary>
        /// the Maintenance Contract associated with this operation sheet
        /// </summary>
        public string MaintenanceContractId { get; set; }

        /// <summary>
        /// the equipments associated with this Maintenance Operation Sheet, only check for this if the <see cref="Type"/> is <see cref="MaintenanceOperationSheetType.Maintenance"/>
        /// </summary>
        public ICollection<MaintenanceContractEquipmentDetails> EquipmentDetails { get; set; }

        /// <summary>
        /// the order details associated with this Maintenance Operation Sheet, only check for this if the <see cref="Type"/> is <see cref="MaintenanceOperationSheetType.AfterSalesService"/>
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the Data sheets associated with the OperationSheet
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of emails that has been sent for this documents
        /// </summary>
        public ICollection<DocumentEmail> Emails { get; set; }

        /// <summary>
        /// entity changes history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// list of Technician Observation
        /// </summary>
        public ICollection<TechnicianObservation> Observations { get; set; }

        /// <summary>
        /// list of associated documents
        /// </summary>
        public ICollection<AssociatedDocument> AssociatedDocuments { get; set; }

        /// <summary>
        /// the technicien signature
        /// </summary>
        public Signature TechnicianSignature { get; set; }

        /// <summary>
        /// the Signature of the client
        /// </summary>
        public Signature ClientSignature { get; set; }

        /// <summary>
        /// the list of UnDone motifs
        /// </summary>
        public ICollection<MaintenanceOperationSheetMotif> UnDoneMotif { get; set; }
    }
}
