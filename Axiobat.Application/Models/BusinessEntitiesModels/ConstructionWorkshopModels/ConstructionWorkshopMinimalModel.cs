﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;

    /// <summary>
    /// model for <see cref="ConstructionWorkshop"/>
    /// </summary>
    [ModelFor(typeof(ConstructionWorkshop))]
    public partial class ConstructionWorkshopMinimalModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// name of the construction workshop
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the construction workshop status
        /// </summary>
        public string Status { get; set; }
    }
}