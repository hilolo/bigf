﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Entities;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class represent the Billing and treasury
    /// </summary>
    public partial class BillingTreasury
    {
        /// <summary>
        /// create an instant of <see cref="BillingTreasury"/>
        /// </summary>
        public BillingTreasury()
        {
            InvoicesDetails = new InvoicesDetails();
            Expenditures = new Expenditures();
            Margin = new BillingTreasuryMargin();
        }

        /// <summary>
        /// the workshop invoices details
        /// </summary>
        public InvoicesDetails InvoicesDetails { get; set; }

        /// <summary>
        /// the workshop Expenditures
        /// </summary>
        public Expenditures Expenditures { get; set; }

        /// <summary>
        /// the margins
        /// </summary>
        public BillingTreasuryMargin Margin { get; set; }
    }

    /// <summary>
    /// the billing Treasury Margins details
    /// </summary>
    public class BillingTreasuryMargin
    {
        public float MaterialMargin { get; set; }
        public float PercentMaterialMargin { get; set; }
        public float WorkforceMargin { get; set; }
        public float PercentWorkforceMargin { get; set; }
        public float TotalHoldback { get; set; }
        public float PercentTotalHoldback => TotalHoldback / (MaterialMargin + WorkforceMargin) * 100;

        public float Total => MaterialMargin + WorkforceMargin;
        public float Deference { get; set; }
        public float Percent { get; set; }
    }

    /// <summary>
    /// the invoices details
    /// </summary>
    public partial class InvoicesDetails
    {
        /// <summary>
        /// the total paid invoices
        /// </summary>
        public float TotalPaid { get; set; }

        /// <summary>
        /// the total invoices rest to pay
        /// </summary>
        public float TotalRestToPay { get; set; }

        /// <summary>
        /// the total of the invoices
        /// </summary>
        public float Total { get; set; }

        /// <summary>
        /// the deference between the billing/treasury and predictions
        /// </summary>
        public float Deference { get; set; }

        /// <summary>
        /// the total of material sales
        /// </summary>
        public float TotalMaterialSales { get; set; }

        /// <summary>
        /// the total of workforce sales
        /// </summary>
        public float TotalWorkforceSales { get; set; }
    }

    /// <summary>
    /// the workshop expenditures
    /// </summary>
    public partial class Expenditures
    {
        /// <summary>
        /// create an instant of <see cref="Expenditures"/>
        /// </summary>
        public Expenditures()
        {
            ExpenseDetails = new HashSet<ExpendituresExpenseDetails>();
            OperationSheetDetails = new ExpendituresOperationSheetDetails();
        }

        /// <summary>
        /// the total amount of the expenses
        /// </summary>
        public float TotalExpenses { get; set; }

        /// <summary>
        /// the percent of the Expenses out of the Expenditures total
        /// </summary>
        public float PercentExpenses { get; set; }

        /// <summary>
        /// the expenses details
        /// </summary>
        public ICollection<ExpendituresExpenseDetails> ExpenseDetails { get; set; }

        /// <summary>
        /// the operation sheet details
        /// </summary>
        public ExpendituresOperationSheetDetails OperationSheetDetails { get; set; }

        /// <summary>
        /// the total of the Expenditures
        /// </summary>
        public float Total => TotalExpenses + OperationSheetDetails.Total;
    }

    /// <summary>
    /// the Expenditures Operation Sheet Details
    /// </summary>
    public partial class ExpendituresOperationSheetDetails
    {
        public float Total => TotalBasketsCost + TotalVisitsCost + TotalWorkingHoursCost;

        public float TotalWorkingHours { get; set; }
        public float TotalBaskets { get; set; }
        public float TotalVisits { get; set; }
        public float TotalVisitsCost { get; set; }
        public float TotalBasketsCost { get; set; }
        public float TotalWorkingHoursCost { get; set; }
        public float Percent { get; set; }
        public float Deference { get; set; }
    }

    /// <summary>
    /// the Expenditures Expense Details
    /// </summary>
    public partial class ExpendituresExpenseDetails
    {
        /// <summary>
        /// the total of this expense
        /// </summary>
        public float Total { get; set; }

        /// <summary>
        /// the total paid expenses for this category
        /// </summary>
        public float TotalPaid { get; set; }

        /// <summary>
        /// the rest to pay amount
        /// </summary>
        public float RestToPay { get; set; }

        /// <summary>
        /// the percent of the paid
        /// </summary>
        public float PercentTotalPaid => TotalPaid / Total * 100;

        /// <summary>
        /// the percent of the rest to pay
        /// </summary>
        public float PercentRestToPay => RestToPay / Total * 100;

        /// <summary>
        /// the percent of this expense out of the total expenses amount
        /// </summary>
        public float Percent { get; set; }

        /// <summary>
        /// the deference value
        /// </summary>
        public float Deference { get; set; }

        /// <summary>
        /// the product category type
        /// </summary>
        public ProductCategoryTypeModel ProductCategoryType { get; set; }
    }
}
