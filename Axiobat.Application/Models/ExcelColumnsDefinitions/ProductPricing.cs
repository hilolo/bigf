﻿namespace Axiobat.Application.Models.DataImport
{
    public class ProductPricing
    {
        public string ProductReference { get; set; }
        public string SupplierReference { get; set; }
        public float Price { get; set; }
        public bool IsDefault { get; set; }
    }
}