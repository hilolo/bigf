﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;

    /// <summary>
    /// the minimal model for <see cref="Permission"/>
    /// </summary>
    [ModelFor(typeof(Permission))]
    public class PermissionModel
    {
        /// <summary>
        /// permission id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// name of the role access
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// a short description to define the Permission
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the id of the module
        /// </summary>
        public AppModules Module { get; set; }

        /// <summary>
        /// get the string Application of the entity
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"Permission name: {Name}, id: {Id}";
    }
}
