﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// The model for the <see cref="Role"/> entity
    /// </summary>
    [ModelFor(typeof(Role))]
    public class RoleUpdateModel : IUpdateModel<Role>
    {
        /// <summary>
        /// name of the role
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// update the entity from the current model instant
        /// </summary>
        /// <param name="entity">the role entity</param>
        public void Update(Role entity)
        {
            entity.Name = Name;
        }

        /// <summary>
        /// get the string Application of the entity
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"role name: {Name}";
    }
}
