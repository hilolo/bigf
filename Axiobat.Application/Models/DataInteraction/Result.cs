﻿namespace Axiobat.Application.Models
{
    using Application.Enums;
    using Domain.Exceptions;
    using System.Collections.Generic;

    /// <summary>
    /// the base result class
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    public partial class Result
    {
        /// <summary>
        /// construct a <see cref="Result"/> instant with default values
        /// </summary>
        public Result()
        {
            Status = ResultStatus.Succeed;
            Message = "Operation Succeeded";
            MessageCode = MessageCode.OperationSucceeded;
            Error = null;
        }

        /// <summary>
        /// the Status of the result
        /// </summary>
        public ResultStatus Status { get; set; }

        /// <summary>
        /// get the message associated with this result, in case of an error
        /// this property will hold the error description
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// a code that represent a message, used for multilingual scenario
        /// </summary>
        public MessageCode MessageCode { get; set; }

        /// <summary>
        /// the exception instant, if there is an error associated with operation
        /// if no error this will be null
        /// </summary>
        public Error Error { get; set; }

        /// <summary>
        /// check if the operation associated with this result has produce a value
        /// </summary>
        public virtual bool HasValue => false;

        /// <summary>
        /// is this Result has raised an error
        /// </summary>
        public bool HasError => !(Error is null);

        /// <summary>
        /// if there is no errors and the status of the result is Success this will be true
        /// </summary>
        public bool IsSuccess => Status == ResultStatus.Succeed && !HasError;

        /// <summary>
        /// get the string reApplication of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Status: {Status}, HasValue: {HasValue}";
    }

    /// <summary>
    /// the partial part of <see cref="Result"/>
    /// </summary>
    public partial class Result
    {
        /// <summary>
        /// create an instant of <see cref="Result"/> form the given result
        /// </summary>
        /// <param name="result">the result to clone</param>
        /// <returns>an instant of <see cref="Result"/></returns>
        public static Result Clone(Result result)
            => new Result()
            {
                Status = result.Status,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
            };

        /// <summary>
        /// create a new Success <see cref="Result"/> with a message of "Operation Succeeded", and
        /// a <see cref="Axiobat.MessageCode"/> of <see cref="MessageCode.OperationSucceeded"/>
        /// </summary>
        /// <returns>an instant of <see cref="Result"/></returns>
        public static Result Success() => new Result();

        /// <summary>
        /// create a new success <see cref="Result"/> with a status of <see cref="ResultStatus.Succeed"/>
        /// and a <see cref="MessageCode"/> of <see cref="MessageCode.OperationSucceeded"/>
        /// </summary>
        /// <param name="message">the message associated with the <see cref="Result"/> instant</param>
        /// <returns>the <see cref="Result"/> instant</returns>
        public static Result Success(string message)
            => new Result()
            {
                Message = message,
                MessageCode = MessageCode.OperationSucceeded,
                Status = ResultStatus.Succeed,
            };

        /// <summary>
        /// create a new success <see cref="Result"/> with a status of <see cref="ResultStatus.Succeed"/>
        /// </summary>
        /// <param name="message">the message associated with the <see cref="Result"/> instant</param>
        /// <param name="messageCode">the <see cref="Axiobat.MessageCode"/> associated with the result</param>
        /// <returns>the <see cref="Result"/> instant</returns>
        public static Result Success(string message, MessageCode codeMessage)
            => new Result()
            {
                Message = message,
                MessageCode = codeMessage,
                Status = ResultStatus.Succeed,
            };

        /// <summary>
        /// create a Failed <see cref="Result"/> with a status of <see cref="ResultStatus.Failed"/>
        /// </summary>
        /// <param name="message">the message associated with the <see cref="Result"/> instant, defaulted to "Operation Failed"</param>
        /// <param name="codeMessage">the <see cref="Axiobat.MessageCode"/>, defaulted to <see cref="MessageCode.OperationFailed"/></param>
        /// <param name="error">the <see cref="Domain.Exceptions.Error"/> associated with the <see cref="Result"/> instant, if any defaulted to <see cref="null"/></param>
        /// <returns></returns>
        public static Result Failed(string message = "Operation Failed", MessageCode codeMessage = MessageCode.OperationFailed, Error error = default)
            => new Result()
            {
                Status = ResultStatus.Failed,
                MessageCode = codeMessage,
                Message = message,
                Error = error,
            };

        /// <summary>
        /// create a new Success <see cref="Result{TResult}"/> with a status of <see cref="ResultStatus.Succeed"/>
        /// </summary>
        /// <param name="value">the result value</param>
        /// <param name="message">the message associated with the <see cref="Result{TResult}"/> instant</param>
        /// <param name="codeMessage">the <see cref="Axiobat.MessageCode"/>, defaulted to <see cref="MessageCode.OperationSucceeded"/></param>
        /// <returns>the <see cref="Result{TResult}"/> instant</returns>
        public static Result<TResult> Success<TResult>(TResult value, string message = "Operation Succeeded", MessageCode codeMessage = MessageCode.OperationSucceeded)
            => new Result<TResult>()
            {
                Status = ResultStatus.Succeed,
                MessageCode = codeMessage,
                Message = message,
                Value = value,
            };

        /// <summary>
        /// create a Failed <see cref="Result"/> with a status of <see cref="ResultStatus.Failed"/>
        /// </summary>
        /// <typeparam name="TResult">Type of the result value</typeparam>
        /// <param name="message">the message associated with the <see cref="Result"/> instant defaulted to "Operation Failed"</param>
        /// <param name="codeMessage">the <see cref="Axiobat.MessageCode"/>, defaulted to <see cref="MessageCode.OperationFailed"/></param>
        /// <param name="error">the <see cref="Domain.Exceptions.Error"/> associated with the <see cref="Result"/> instant, if any defaulted to <see cref="null"/></param>
        /// <returns>an instant of <see cref="Result{TResult}"/></returns>
        public static Result<TResult> Failed<TResult>(string message = "Operation Failed", MessageCode codeMessage = MessageCode.OperationFailed, Error error = default)
            => new Result<TResult>()
            {
                Value = default,
                Status = ResultStatus.Failed,
                Message = message,
                MessageCode = codeMessage,
                Error = error,
            };

        /// <summary>
        /// create a new Success result with message "Operation Succeeded" and message code of "1"
        /// </summary>
        /// <param name="value">the result value</param>
        /// <returns>the <see cref="ListResult{TResult}"/> instant</returns>
        public static ListResult<TResult> ListSuccess<TResult>(IEnumerable<TResult> value)
            => new ListResult<TResult>()
            {
                Status = ResultStatus.Succeed,
                Message = "Operation Succeeded",
                MessageCode = MessageCode.OperationSucceeded,
                Value = value,
            };

        /// <summary>
        /// create a new Failed result with message "Operation Failed" and message code of "2"
        /// </summary>
        /// <param name="value">the result value</param>
        /// <returns>the <see cref="ListResult{TResult}"/> instant</returns>
        public static ListResult<TResult> ListFailed<TResult>()
            => new ListResult<TResult>()
            {
                Value = default,
                Status = ResultStatus.Failed,
                Message = "Operation Failed",
                MessageCode = MessageCode.OperationFailed,
                Error = null,
            };

        /// <summary>
        /// create a new Failed result with 'Operation Failed' message
        /// also a messageCode = 2, and status of <see cref="ResultStatus.Succeed"/>
        /// </summary>
        public static ListResult<TResult> ListFailed<TResult>(string message, MessageCode codeMessage, Error error = default)
            => new ListResult<TResult>()
            {
                Value = default,
                Status = ResultStatus.Failed,
                Message = message,
                MessageCode = codeMessage,
                Error = error,
            };

        /// <summary>
        /// create a new Success result with message "Operation Succeeded" and message code of "1"
        /// </summary>
        /// <param name="value">the result value</param>
        /// <returns>the <see cref="ListResult{TResult}"/> instant</returns>
        public static PagedResult<TResult> PagedSuccess<TResult>(IEnumerable<TResult> value, int currentPage, int pageSize, int rowsCount)
            => new PagedResult<TResult>()
            {
                Status = ResultStatus.Succeed,
                Message = "Operation Succeeded",
                MessageCode = MessageCode.OperationSucceeded,
                Value = value,
                CurrentPage = currentPage,
                PageSize = pageSize,
                RowsCount = rowsCount,
            };

        /// <summary>
        /// create a new Success result with message "Operation Succeeded" and message code of "1"
        /// </summary>
        /// <param name="value">the result value</param>
        /// <returns>the <see cref="ListResult{TResult}"/> instant</returns>
        public static PagedResult<TOut> PagedSuccess<TOut, TResult>(IEnumerable<TOut> value, PagedResult<TResult> pagedResult)
            => new PagedResult<TOut>()
            {
                Status = ResultStatus.Succeed,
                Message = "Operation Succeeded",
                MessageCode = MessageCode.OperationSucceeded,
                Value = value,
                CurrentPage = pagedResult.CurrentPage,
                PageSize = pagedResult.PageSize,
                RowsCount = pagedResult.RowsCount,
            };

        /// <summary>
        /// create a new Failed result with message "Operation Failed" and message code of "2"
        /// </summary>
        /// <param name="value">the result value</param>
        /// <returns>the <see cref="ListResult{TResult}"/> instant</returns>
        public static PagedResult<TResult> PagedFailed<TResult>()
            => new PagedResult<TResult>()
            {
                Value = default,
                Status = ResultStatus.Failed,
                Message = "Operation Failed",
                MessageCode = MessageCode.OperationFailed,
                Error = null,
                CurrentPage = 1,
                PageSize = 10,
                RowsCount = 0,
            };

        /// <summary>
        /// create a new Failed result with 'Operation Failed' message
        /// also a messageCode = 2, and status of <see cref="ResultStatus.Succeed"/>
        /// </summary>
        public static PagedResult<TResult> PagedFailed<TResult>(string message, MessageCode codeMessage, Error error = default)
            => new PagedResult<TResult>()
            {
                Value = default,
                Status = ResultStatus.Failed,
                Message = message,
                MessageCode = codeMessage,
                Error = error,
                CurrentPage = 1,
                PageSize = 10,
                RowsCount = 0,
            };

        /// <summary>
        /// build a <see cref="Result"/> form the given result instant
        /// </summary>
        /// <param name="result">the type of the result</param>
        /// <returns>the result instant</returns>
        public static Result<TOut> From<TOut>(Result result)
            => new Result<TOut>()
            {
                Value = default,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                Status = result.Status
            };

        /// <summary>
        /// build a <see cref="Result"/> instant from the given <see cref="Result{TResult}"/>
        /// </summary>
        /// <typeparam name="TResult">the type of th result</typeparam>
        /// <param name="result">the <see cref="Result{TResult}"/> instant</param>
        /// <returns>the generated <see cref="Result"/></returns>
        public static Result From<TResult>(Result<TResult> result)
            => new Result()
            {
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                Status = result.Status
            };

        /// <summary>
        /// build a <see cref="Result"/> form the given result instant
        /// </summary>
        /// <param name="result">the type of the result</param>
        /// <returns>the result instant</returns>
        public static Result<TOut> From<TOut, TResult>(Result<TResult> result)
            => new Result<TOut>()
            {
                Value = default,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                Status = result.Status
            };

        /// <summary>
        /// build a <see cref="Result"/> form the given result instant
        /// </summary>
        /// <param name="result">the type of the result</param>
        /// <returns>the result instant</returns>
        public static ListResult<TOut> ListFrom<TOut, TResult>(Result<TResult> result)
            => new ListResult<TOut>()
            {
                Value = default,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                Status = result.Status
            };

        /// <summary>
        /// build a <see cref="Result{TResult}"/> form the given <see cref="ListResult{TResult}"/>
        /// </summary>
        /// <param name="result">the <see cref="ListResult{TResult}"/> instant</param>
        /// <returns>the <see cref="Result{TResult}"/> instant</returns>
        public static ListResult<TOut> ListFrom<TOut, TResult>(ListResult<TResult> result)
            => new ListResult<TOut>()
            {
                Value = default,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                Status = result.Status
            };

        /// <summary>
        /// build a <see cref="Result{TResult}"/> form the given <see cref="ListResult{TResult}"/>
        /// </summary>
        /// <param name="result">the <see cref="ListResult{TResult}"/> instant</param>
        /// <returns>the <see cref="Result{TResult}"/> instant</returns>
        public static PagedResult<TOut> ListFrom<TOut, TResult>(PagedResult<TResult> result)
            => new PagedResult<TOut>()
            {
                Value = default,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                Status = result.Status
            };

        /// <summary>
        /// build a <see cref="Result{TResult}"/> form the given <see cref="ListResult{TResult}"/>
        /// </summary>
        /// <param name="result">the <see cref="ListResult{TResult}"/> instant</param>
        /// <returns>the <see cref="Result{TResult}"/> instant</returns>
        public static Result<TOut> From<TOut, TResult>(ListResult<TResult> result)
            => new Result<TOut>()
            {
                Value = default,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                Status = result.Status
            };

        /// <summary>
        /// build a <see cref="Result{TResult}"/> form the given <see cref="PagedResult{TResult}"/>
        /// </summary>
        /// <param name="result">the <see cref="PagedResult{TResult}"/> instant</param>
        /// <returns>the <see cref="Result{TResult}"/> instant</returns>
        public static Result<TOut> From<TOut, TResult>(PagedResult<TResult> result)
            => new Result<TOut>()
            {
                Value = default,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                Status = result.Status
            };
    }

    /// <summary>
    /// the result class with a value property
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    public class Result<TResult> : Result
    {
        /// <summary>
        /// construct a <see cref="Result{TResult}"/> instant with default values
        /// </summary>
        public Result() : base() => Value = default;

        /// <summary>
        /// the result of the operation
        /// </summary>
        public TResult Value { get; set; }

        /// <summary>
        /// check if the operation associated with this result has produce a value
        /// </summary>
        public override bool HasValue
            => !EqualityComparer<TResult>.Default.Equals(Value, default);

        /// <summary>
        /// get the string reApplication of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"{base.ToString()}, Value: {Value}";

        /// <summary>
        /// create an instant of <see cref="Result{TResult}"/> form the given result
        /// </summary>
        /// <param name="result">the result to clone</param>
        /// <returns>an instant on <see cref="Result{TResult}"/></returns>
        public static new Result<TResult> Clone(Result result)
            => new Result<TResult>()
            {
                Status = result.Status,
                Error = result.Error,
                Message = result.Message,
                MessageCode = result.MessageCode,
                Value = default
            };

        #region Operators overrides

        public static implicit operator TResult(Result<TResult> result)
            => result is null || !result.HasValue ? (default) : result.Value;

        public static implicit operator Result<TResult>(TResult result)
            => Success(result);

        #endregion
    }
}
