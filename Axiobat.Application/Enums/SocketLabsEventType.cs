﻿namespace Axiobat.Application.Enums
{
    /// <summary>
    /// Enumeration of the Available Event Types for <see cref="Models.SocketLabsEvent"/>
    /// </summary>
    public enum SocketLabsEventType
    {
        /// <summary>
        /// message has been delivered
        /// </summary>
        Delivered,

        /// <summary>
        /// a Complaint has been issued
        /// </summary>
        Complaint,

        /// <summary>
        /// failed to send a message
        /// </summary>
        Failed,

        /// <summary>
        /// server validation
        /// </summary>
        Validation,

        /// <summary>
        /// tracking event
        /// </summary>
        Tracking,

        /// <summary>
        /// unknown event
        /// </summary>
        Unknown
    }
}
