﻿namespace Axiobat.Application.Data
{
    using Models;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the dashboard data access
    /// </summary>
    public interface IDashboardDataAccess
    {
        /// <summary>
        /// get the list of quotes for documents details
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>list of quotes</returns>
        Task<Quote[]> GetQuotesForDocumentDetailsAsync(DateRangeFilterOptions filterOptions);

        /// <summary>
        /// get the invoices For documents details
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>the list of invoices</returns>
        Task<Invoice[]> GetInvoicesForDocumentDetailsAsync(DateRangeFilterOptions filterOptions);

        Task<ConstructionWorkshop[]> GetWorkshopsForDocumentDetailsAsync(DateRangeFilterOptions filterOptions);
        Task<MaintenanceContract[]> GetContractsForDocumentDetailsAsync(DateRangeFilterOptions filterOptions);
        Task<OperationSheet[]> GetOperationSheetForDocumentDetailsAsync(DateRangeFilterOptions filterOptions);
        Task<MaintenanceOperationSheet[]> GetOperationSheetMaintenceForDocumentDetailsAsync(DateRangeFilterOptions filterOptions);

        Task<ConstructionWorkshop[]> GetWorkshopsForTopsDetailsAsync();
        Task<MaintenanceContract[]> GetContractsForTopsDetailsAsync();
        Task<Invoice[]> GetInvoicesForTopsDetailsAsync();
        Task<CreditNote[]> GetCreditNotesForTopsDetailsAsync();
        Task<Expense[]> GetExpensesForTopsDetailsAsync();

        Task<Expense[]> GetExpensesForDetailsAsync(int year);

        Task<Expense[]> GetExpensesForCashSummaryDetailsAsync();
        Task<Invoice[]> GetInvoicesForCashSummaryDetailsAsync();

        Task<Invoice[]> GetInvoicesForTurnOverDetailsAsync(int year);
        Task<CreditNote[]> GetCreditNotesForTurnOverDetailsAsync(int year);
        Task<Quote[]> GetQuotesForTurnOverDetailsAsync(int year);
    }
}
