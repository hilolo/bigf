﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="Invoice"/>
    /// </summary>
    public interface IExpenseDataAccess : IDocumentDataAccess<Expense>
    {
        /// <summary>
        /// check if the expense has any payments associated with it
        /// </summary>
        /// <param name="expenseId">the id of the expense</param>
        /// <returns>true if any payments exist, false if not</returns>
        Task<bool> HasPaymentsAsync(string expenseId);
    }
}
