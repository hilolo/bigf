﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="OperationSheet"/>
    /// </summary>
    public interface IOperationSheetDataAccess : IDataAccess<OperationSheet, string>
    {
        /// <summary>
        /// remove the operation sheet technicians
        /// </summary>
        /// <param name="removed">the list of operation sheets to remove</param>
        Task RemoveOperationSheetTechniciansAsync(IEnumerable<OperationSheets_Technicians> removed);

        /// <summary>
        /// add the operation sheet technicians
        /// </summary>
        /// <param name="added">the list of operation sheets to be added</param>
        Task AddOperationSheetTechniciansAsync(IEnumerable<OperationSheets_Technicians> added);

        /// <summary>
        /// get list of operation sheets owned by the given client
        /// </summary>
        /// <param name="clientId">the id of the client</param>
        /// <returns>the list of operation sheets</returns>
        Task<IEnumerable<OperationSheet>> GetOperationSheetsByClientIdAsync(string clientId);

        /// <summary>
        /// update the status of all operation sheets associated with the given invoice
        /// </summary>
        /// <param name="invoiceId">the id of the invoice</param>
        /// <param name="status">the new status</param>
        Task UpdateInvoiceOperationSheetStatusAsync(string invoiceId, string status);

        /// <summary>
        /// get the operation sheets of the given technician
        /// </summary>
        /// <param name="technicianId">the id of the technician</param>
        /// <returns>list of operation sheets</returns>
        Task<IEnumerable<OperationSheet>> GetTechnicianOperationSheetsAsync(Guid technicianId);
        Task<OperationSheet[]> GetNewestObjectsAsync(DateTimeOffset? lastSyncDate, string[] syncObjectsIds);
        Task<OperationSheet[]> GetFirstsynchronizationDataAsync();
    }
}
