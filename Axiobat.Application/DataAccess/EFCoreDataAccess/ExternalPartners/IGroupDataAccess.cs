﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the data access for <see cref="Group"/> entity
    /// </summary>
    public interface IGroupDataAccess : IDataAccess<Group, int>
    {

    }
}
