﻿namespace Axiobat.Application.Data
{
    using System.Threading.Tasks;
    using Application.Models;
    using Domain.Entities;

    /// <summary>
    /// the data access for <see cref="GlobalHistory"/>
    /// </summary>
    public interface IGlobalHistoryDataAccess : IDataAccess<GlobalHistory, int>
    {
        
    }
}