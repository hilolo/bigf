﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the <see cref="ChartAccountItem"/> DataAccess
    /// </summary>
    public interface IChartOfAccountsDataAccess : IDataAccess<ChartAccountItem, string>
    {

    }
}
