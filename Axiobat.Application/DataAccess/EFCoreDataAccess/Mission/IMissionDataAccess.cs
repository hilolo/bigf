﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the data access for <see cref="Mission"/>
    /// </summary>
    public interface IMissionDataAccess : IDataAccess<Mission, string>
    {

    }

    /// <summary>
    /// the data access for <see cref="MissionType"/>
    /// </summary>
    public interface IMissionTypeDataAccess : IDataAccess<MissionType, int>
    {

    }
}
