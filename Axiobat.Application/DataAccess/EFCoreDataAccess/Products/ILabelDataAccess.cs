﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the data access for <see cref="Label"/>
    /// </summary>
    public interface ILabelDataAccess : IDataAccess<Label, string>
    {

    }
}
