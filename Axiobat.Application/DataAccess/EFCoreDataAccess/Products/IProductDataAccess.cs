﻿namespace Axiobat.Application.Data
{
    using Application.Models;
    using Domain.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="Product"/>
    /// </summary>
    public interface IProductDataAccess : IProductsBaseDataAccess<Product>
    {
        /// <summary>
        /// add the product suppliers relationships
        /// </summary>
        /// <param name="supplierProdcuts">the supplier product</param>
        /// <returns>the operations result</returns>
        Task<Result> AddProductsSuppliersAsync(params ProductSupplier[] supplierProdcuts);

        /// <summary>
        /// add the products labels
        /// </summary>
        /// <param name="productLables">the product labels</param>
        /// <returns>the operations result</returns>
        Task<Result> AddProductLabelsAsync(params ProductLabel[] productLables);

        /// <summary>
        /// delete the product suppliers
        /// </summary>
        /// <param name="productId">the id of the product</param>
        Task DeleteProductSuppliersAsync(string productId);

        /// <summary>
        /// check if the given category type id belongs to an existing category
        /// </summary>
        /// <param name="categoryTypeId">the id of the category to check</param>
        /// <returns>true if exist, false if not</ret
        Task<bool> IsProductCategoryTypeExistAsync(string categoryTypeId);

        /// <summary>
        /// delete the product labels
        /// </summary>
        /// <param name="productId">the id of the product</param>
        Task DeleProductLabelsAsync(string productId);

        /// <summary>
        /// get the list of the product categories types
        /// </summary>
        /// <returns>list of categories types</returns>
        Task<IEnumerable<ProductCategoryType>> GetProductCategoriesTypeAsync();

        /// <summary>
        /// retrieve list of product by supplier Id
        /// </summary>
        /// <param name="supplierId">the id of the supplier</param>
        /// <returns>the list of products</returns>
        Task<IEnumerable<Product>> GetBySupplierIdAsync(string supplierId);
    }
}
