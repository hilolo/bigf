﻿namespace Axiobat
{
    /// <summary>
    /// a predefined Template Messages
    /// </summary>
    public static class TemplateMessages
    {
        /// <summary>
        /// the template message for assigning a new operation sheet to a technicien
        /// </summary>
        public const string OperationSheetAssignment = "operation_sheet_assignment";
    }

    /// <summary>
    /// a predefined Place Holders
    /// </summary>
    public static class PlaceHolders
    {
        /// <summary>
        /// a place holder for the client name
        /// </summary>
        public const string ClientName = "##client_name##";

        /// <summary>
        /// a place holder for a reference
        /// </summary>
        public const string reference = "##reference##";

        /// <summary>
        /// a place holder for a Start Date
        /// </summary>
        public const string StartDate = "##start_date##";
    }
}