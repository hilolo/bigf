﻿namespace Axiobat.Presentation.Controllers.Configuration
{
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Configuration;
    using Domain.Entities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the <see cref="Country"/> controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class CountryController : BaseController<Country>
    {
        /// <summary>
        /// get the list of all countries
        /// </summary>
        /// <returns>list of countries</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PagedResult<Country>>> GetAllAsync(
            [FromBody] FilterOptions filter)
        {
            return ActionResultFor(await _service.GetAsPagedResultAsync<Country, FilterOptions>(filter));
        }

        /// <summary>
        /// get the country with the given country code
        /// </summary>
        /// <param name="countryCode">the country code to be retrieved</param>
        /// <returns>the country if any</returns>
        [HttpGet("{countryCode}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<Country>>> GetByCodeAsync([FromRoute] string countryCode)
        {
            return ActionResultFor((await _service.GetByCodeAsync(countryCode)));
        }

        /// <summary>
        /// get the country with the given name
        /// </summary>
        /// <param name="countryName">the country name to be retrieved</param>
        /// <returns>the country if any</returns>
        [HttpGet("{countryName}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<Country>>> GetByNameAsync([FromRoute] string countryName)
        {
            return ActionResultFor(await _service.GetByNameAsync(countryName));
        }
    }

    /// <summary>
    /// partial part for <see cref="CountryController"/>
    /// </summary>
    public partial class CountryController
    {
        private readonly ICountryService _service;

        /// <summary>
        /// create an instant of <see cref="CountryController"/>
        /// </summary>
        /// <param name="countryService">the <see cref="ICountryService"/> instant</param>
        /// <param name="loggerFactory">the <see cref="ILoggerFactory"/> instant</param>
        /// <param name="loggedInUserService">the <see cref="ILoggedInUserService"/> instant</param>
        /// <param name="translationService">the <see cref="ITranslationService"/> instant</param>
        public CountryController(
            ICountryService countryService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = countryService;
        }
    }
}