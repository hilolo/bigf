﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Documents;
    using Application.Services.Localization;
    using Axiobat.Application.Services.Maintenance;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the OperationSheet API Controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class OperationSheetController : BaseController<OperationSheet>
    {
        /// <summary>
        /// get list of all Operation Sheet
        /// </summary>
        /// <returns>list of all operation sheets</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<OperationSheetListModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<OperationSheetListModel>());

        /// <summary>
        /// get list of all Operation Sheet owned by the given client
        /// </summary>
        /// <param name="clientId">the id of the client to retrieve the list of operation sheets for it</param>
        /// <returns>list of all operation sheets</returns>
        [HttpGet("client/{clientId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<OperationSheetListModel>>> GetAll([FromRoute(Name = "clientId")] string clientId)
            => ActionResultForAsync(_service.GetClientOperationSheetsAsync<OperationSheetListModel>(clientId));

        /// <summary>
        /// get a paged result of the OperationSheets list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of OperationSheets as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<OperationSheetListModel>>> Get([FromBody] OperationSheetFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<OperationSheetListModel, OperationSheetFilterOptions>(filter));

        /// <summary>
        /// get a paged result of the OperationSheets list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of OperationSheets as paged result</returns>
        [HttpPost("All")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PagedResult<CombinedOperationSheetListModel>>> GetAll(
            [FromBody] MaintenanceOperationSheetFilterOptions filter,
            [FromServices] IMaintenanceOperationSheetService maintenanceOperationSheetService)
        {
            var operationSheet = await _service
                .GetAsPagedResultAsync<CombinedOperationSheetListModel, OperationSheetFilterOptions>(filter);

            var operationSheetMaiantence = await maintenanceOperationSheetService
                .GetAsPagedResultAsync<CombinedOperationSheetListModel, MaintenanceOperationSheetFilterOptions>(filter);

            var combinedList = operationSheet
                .Value.Concat(operationSheetMaiantence.Value)
                .OrderByDescending(e => e.StartDate);
                //.Take(filter.PageSize);

            var rowsCount = operationSheet.RowsCount + operationSheetMaiantence.RowsCount;

            return Result.PagedSuccess(
                value      : combinedList,
                currentPage: filter.Page,
                pageSize   : filter.PageSize,
                rowsCount  : rowsCount);
        }

        /// <summary>
        /// retrieve OperationSheet with the given reference
        /// </summary>
        /// <param name="operationSheetReference">the reference of the operation sheet to be retrieved</param>
        /// <returns>the OperationSheet</returns>
        [HttpGet("Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<OperationSheetModel>>> GetByReference([FromRoute(Name = "reference")] string operationSheetReference)
            => ActionResultForAsync(_service.GetByReferenceAsync<OperationSheetModel>(operationSheetReference));

        /// <summary>
        /// retrieve OperationSheet with the given id
        /// </summary>
        /// <param name="operationSheetId">the id of the OperationSheet to be retrieved</param>
        /// <returns>the OperationSheet</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<OperationSheetModel>>> Get([FromRoute(Name = "id")] string operationSheetId)
            => ActionResultForAsync(_service.GetByIdAsync<OperationSheetModel>(operationSheetId));

        /// <summary>
        /// create a new OperationSheet record
        /// </summary>
        /// <param name="OperationSheetModel">the model to create the OperationSheet from it</param>
        /// <returns>the newly created OperationSheet</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<OperationSheetModel>>> Create(
            [FromBody] OperationSheetPutModel OperationSheetModel)
        {
            var result = await _service.CreateAsync<OperationSheetModel, OperationSheetPutModel>(OperationSheetModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// send the operationSheet with the given id via an email using the given options
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to be sent</param>
        /// <returns>the operation result</returns>
        [HttpPost("{id}/Email")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> EmailDocument([FromRoute(Name = "id")] string operationSheetId, [FromBody] SendEmailOptions emailOptions)
            => ActionResultForAsync(_service.SendAsync(operationSheetId, emailOptions));

        /// <summary>
        /// update the OperationSheet informations
        /// </summary>
        /// <param name="OperationSheetModel">the model to use for updating the OperationSheet</param>
        /// <param name="OperationSheetId">the id of the OperationSheet to be updated</param>
        /// <returns>the updated OperationSheet</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<OperationSheetModel>>> Update([FromBody] OperationSheetPutModel OperationSheetModel, [FromRoute(Name = "id")] string OperationSheetId)
            => ActionResultForAsync(_service.UpdateAsync<OperationSheetModel, OperationSheetPutModel>(OperationSheetId, OperationSheetModel));

        /// <summary>
        /// delete the OperationSheet with the given id
        /// </summary>
        /// <param name="OperationSheetId">the id of the OperationSheet to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string OperationSheetId)
            => ActionResultForAsync(_service.DeleteAsync(OperationSheetId));

        /// <summary>
        /// update the Operation Sheet status
        /// </summary>
        /// <param name="model">the model to use for updating the Operation Sheet status</param>
        /// <param name="operationSheetId">the id of the Operation Sheet to be updated</param>
        /// <returns>the updated Operation Sheet</returns>
        [HttpPut("{id}/Update/Status")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdateStatus([FromBody] DocumentUpdateStatusModel model,
            [FromRoute(Name = "id")] string operationSheetId)
            => ActionResultForAsync(_service.UpdateStatusAsync(operationSheetId, model));


        /// <summary>
        /// update the Operation Sheet status
        /// </summary>
        /// <param name="model">the model to use for updating the Operation Sheet status</param>
        /// <param name="operationSheetId">the id of the Operation Sheet to be updated</param>
        /// <returns>the updated Operation Sheet</returns>
        [HttpPut("{id}/Update/ModeStatus")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdateModeStatus([FromBody] ModeStatut model,
            [FromRoute(Name = "id")] string operationSheetId)
            => ActionResultForAsync(_service.UpdateModeStatusAsync(operationSheetId, model));

        /// <summary>
        /// update the signature Sheet status
        /// </summary>
        /// <param name="model">the model to use for updating the Operation Sheet signature</param>
        /// <param name="operationSheetId">the id of the Operation Sheet to be updated</param>
        /// <returns>the updated Operation Sheet</returns>
        [HttpPut("{id}/Update/Signature")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdateSignature([FromBody] DocumentUpdateSignatureModel model,
            [FromRoute(Name = "id")] string operationSheetId)
            => ActionResultForAsync(_service.UpdateSignatureAsync(operationSheetId, model));

        /// <summary>
        /// duplicate the operation Sheet with the given id
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to be duplicated</param>
        /// <returns>the generated operationSheet</returns>
        [HttpGet("Duplicate/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<OperationSheetModel>>> Duplicate([FromRoute(Name = "id")] string operationSheetId)
            => ActionResultForAsync(_service.DuplicateAsync<OperationSheetModel>(operationSheetId));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);

        /// <summary>
        /// export the operation sheet with the given id
        /// </summary>
        /// <param name="OperationSheetID">the id of the operation sheet</param>
        /// <param name="exportOptions">the operation sheet</param>
        /// <returns>generate file as byte array</returns>
        [HttpPost("{id}/Export")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<byte[]>>> Export([FromRoute(Name = "id")] string OperationSheetID, [FromBody] DataExportOptions exportOptions)
            => ActionResultForAsync(_service.ExportDataAsync(OperationSheetID, exportOptions));
    }

    /// <summary>
    /// partial part for <see cref="OperationSheetController"/>
    /// </summary>
    public partial class OperationSheetController : BaseController<OperationSheet>
    {
        private readonly IOperationSheetService _service;

        public OperationSheetController(
            IOperationSheetService OperationSheetService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = OperationSheetService;
        }
    }
}