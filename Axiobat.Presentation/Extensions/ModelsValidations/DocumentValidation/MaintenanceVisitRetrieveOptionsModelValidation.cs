﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using Axiobat.Application.Services.Maintenance;
    using FluentValidation;
    using Microsoft.Extensions.Logging;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// the model validation for <see cref="MaintenanceVisitRetrieveOptions"/>
    /// </summary>
    public class MaintenanceVisitRetrieveOptionsModelValidation : BaseValidator<MaintenanceVisitRetrieveOptions>
    {
        private readonly IMaintenanceContractService _contractService;

        public MaintenanceVisitRetrieveOptionsModelValidation(
            IMaintenanceContractService contractService,
            ILoggerFactory loggerFactory) : base(loggerFactory)
        {
            _contractService = contractService;

            RuleFor(e => e.ContractId)
                .NotNull()
                    .WithMessage("the contract id is required")
                    .WithErrorCode(MessageCode.MaintenanceContractIsRequired)
                .NotEmpty()
                    .WithMessage("the contract id is required")
                    .WithErrorCode(MessageCode.MaintenanceContractIsRequired)
                .MustAsync(ContractExistAsync)
                    .WithMessage("the contract not exist")
                    .WithErrorCode(MessageCode.MaintenanceContractNotExist);

            RuleFor(e => e.Month)
                .Must(month => month >= 1 && month <= 12)
                    .WithMessage("the given Month value is invalid, must be between 1 and 12")
                    .WithErrorCode(MessageCode.InvalidMonth);
        }

        private Task<bool> ContractExistAsync(string contractId, CancellationToken arg2)
            => _contractService.IsExistAsync(contractId);
    }
}
