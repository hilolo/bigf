﻿namespace Axiobat.Presentation.Models.Validations
{
    using App.Common.Exceptions;
    using App.Common.Models;
    using FluentValidation;
    using FluentValidation.AspNetCore;
    using FluentValidation.Results;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// base Validator class
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    public class BaseValidator<TEntity> : AbstractValidator<TEntity>, IValidatorInterceptor
        where TEntity : class
    {
        /// <summary>
        /// the logger
        /// </summary>
        protected readonly ILogger<TEntity> _logger;

        /// <summary>
        /// default constructor with logger
        /// </summary>
        /// <param name="loggerFactory">the logger</param>
        public BaseValidator(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<TEntity>();
        }

        /// <summary>
        /// Invoked Before MVC validation takes place which allows the result to be customized.
        /// It should return a ValidationResult.
        /// </summary>
        /// <param name="controllerContext">controller context</param>
        /// <param name="validationContext">validation context</param>
        /// <returns>validationContext</returns>
        public ValidationContext BeforeMvcValidation(ControllerContext controllerContext, ValidationContext validationContext) => validationContext;

        /// <summary>
        /// Invoked after MVC validation takes place which allows the result to be customized.
        /// It should return a ValidationResult.
        /// </summary>
        /// <param name="controllerContext">controller context</param>
        /// <param name="validationContext">validation context</param>
        /// <param name="result">validation result</param>
        /// <returns>validation result</returns>
        public ValidationResult AfterMvcValidation(ControllerContext controllerContext, ValidationContext validationContext, ValidationResult result)
        {
            if (!result.IsValid)
            {
                var descriptor = new ModelValidationDescriptor();
                descriptor.AddPropertiesErrors(result
                    .Errors
                    .Distinct(ValidationFailureComparer.Instance)
                    .GroupBy(c => c.PropertyName)
                    .Select(o => new PropertyErrorDescriptor()
                    {
                        Property = o.Key,
                        Errors = o.Select(e => new ErrorDescriptor()
                        {
                            Message = e.ErrorMessage,
                            MessageCode = GetErrorCodeFromMessage(e.ErrorCode),
                        }).ToList()
                    }));

                throw new ModelValidationException(descriptor);
            }

            return result;
        }

        private string GetErrorCodeFromMessage(string errorMessage)
        {
            if (Enum.TryParse(errorMessage, true, out MessageCode result))
                return ((int)result).ToString();

            return ((int)MessageCode.ValidationFailed).ToString();
        }
    }
}
