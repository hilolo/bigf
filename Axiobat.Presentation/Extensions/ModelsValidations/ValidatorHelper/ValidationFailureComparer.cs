﻿namespace Axiobat.Presentation.Models.Validations
{
    using FluentValidation.Results;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the IEqualityComparer implementation for ValidationFailure
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    public class ValidationFailureComparer : IEqualityComparer<ValidationFailure>
    {
        private static readonly Lazy<ValidationFailureComparer> _lazy = new Lazy<ValidationFailureComparer>
            (() => new ValidationFailureComparer());

        /// <summary>
        /// return an instant of <see cref="ValidationFailureComparer"/>
        /// </summary>
        public static ValidationFailureComparer Instance { get { return _lazy.Value; } }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type T to compare.</param>
        /// <param name="y">The second object of type T to compare.</param>
        /// <returns>true if the specified objects are equal; otherwise, false.</returns>
        public bool Equals(ValidationFailure x, ValidationFailure y)
        {
            if (x.ErrorCode is null || y.ErrorCode is null)
                return x.ErrorMessage.Equals(y.ErrorMessage, StringComparison.OrdinalIgnoreCase);

            return x.ErrorCode.Equals(y.ErrorCode);
        }

        /// <summary>
        /// Returns a hash code for the specified object.
        /// </summary>
        /// <param name="obj">The Object for which a hash code is to be returned.</param>
        /// <returns>A hash code for the specified object.</returns>
        public int GetHashCode(ValidationFailure obj)
        {
            if (obj.ErrorCode is null)
                return obj.ErrorMessage.GetHashCode();

            return obj.ErrorCode.GetHashCode();
        }
    }
}
