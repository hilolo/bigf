﻿namespace Axiobat.Persistence
{
    /// <summary>
    /// the options for the Persistence layer
    /// </summary>
    public class PersistenceOptions
    {
        /// <summary>
        /// the connection string to the database
        /// </summary>
        public string DatabaseConnectionString { get; set; }
    }
}
