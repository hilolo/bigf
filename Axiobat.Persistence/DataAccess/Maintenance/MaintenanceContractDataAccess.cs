﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Axiobat.Domain.Constants;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// the data access implantation for <see cref="IMaintenanceContractDataAccess"/>
    /// </summary>
    public partial class MaintenanceContractDataAccess
    {
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceContractDataAccess"/>
    /// </summary>
    public partial class MaintenanceContractDataAccess : DataAccess<MaintenanceContract, string>, IMaintenanceContractDataAccess
    {
        public MaintenanceContractDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<MaintenanceContract> SetDefaultIncludsForListRetrieve(IQueryable<MaintenanceContract> query)
            => query
                .Include(e => e.MaintenanceOperationsSheets)
                .Include(e => e.MaintenanceVisits)
                    .ThenInclude(e => e.MaintenanceOperationSheet)
                .Include(e => e.MaintenanceVisits);

        protected override IQueryable<MaintenanceContract> SetDefaultIncludsForSingleRetrieve(IQueryable<MaintenanceContract> query)
            => query
                .Include(e => e.Invoices)
                .Include(e => e.MaintenanceOperationsSheets)
                    .ThenInclude(e => e.Technician)
                .Include(e => e.MaintenanceVisits)
                    .ThenInclude(e => e.MaintenanceOperationSheet);

        protected override IQueryable<MaintenanceContract> SetPagedResultFilterOptions<IFilter>(IQueryable<MaintenanceContract> query, IFilter filterOption)
        {
            if (filterOption is MaintenanceContractFilterOptions filter)
            {
                if (filter.Alerted)
                {
                    query = query.Where(e => e.ExpirationAlertEnabled &&
                        e.EndDate.AddDays(-e.ExpirationAlertPeriod) <= DateTime.Today.Date &&
                        e.EndDate.Date >= DateTime.Today.Date);
                }

                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.StartDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.EndDate < filter.DateEnd);

                if (!(filter.ExternalPartnerId is null) && filter.ExternalPartnerId.Count() > 0)
                    query = query.Where(e => filter.ExternalPartnerId.Contains(e.ClientId));

                if (filter.Status.Any())
                    query = query.Where(GetStatusPredicate(filter.Status));
            }

            if (filterOption is MaintenanceVisitFilterOptions maintenanceVisitfilter)
            {
                if (!(maintenanceVisitfilter.ExternalPartnerId is null) && maintenanceVisitfilter.ExternalPartnerId.Count() > 0)
                    query = query.Where(e => maintenanceVisitfilter.ExternalPartnerId.Contains(e.ClientId));

                if (maintenanceVisitfilter.Year.HasValue)
                    query = query.Where(e => e.StartDate.Year > maintenanceVisitfilter.Year && maintenanceVisitfilter.Year <= e.EndDate.Year);
            }

            return query;
        }

        protected static Expression<Func<MaintenanceContract, bool>> GetStatusPredicate(IEnumerable<string> status)
        {
            var newStatus = new HashSet<string>(status);
            var predicate = PredicateBuilder.True<MaintenanceContract>();
            var isFirst = true;

            if (status.Contains(MaintenanceContractStatus.Finished))
            {
                predicate = predicate.And(e => e.Status == MaintenanceContractStatus.Waiting && e.EndDate.Date <= DateTime.Today);
                newStatus.Remove(MaintenanceContractStatus.Finished);
                isFirst = false;
            }

            if (status.Contains(MaintenanceContractStatus.InProgress))
            {
                if (isFirst)
                    predicate = predicate.And(e => e.Status == MaintenanceContractStatus.Waiting && e.StartDate.Date >= DateTime.Today);
                else
                    predicate = predicate.Or(e => e.Status == MaintenanceContractStatus.Waiting && e.StartDate.Date >= DateTime.Today);

                newStatus.Remove(MaintenanceContractStatus.InProgress);
                isFirst = false;
            }

            if (status.Contains(MaintenanceContractStatus.Waiting))
            {
                if (isFirst)
                    predicate = predicate.And(e => e.Status == MaintenanceContractStatus.Waiting && e.StartDate.Date < DateTime.Today && e.EndDate.Date > DateTime.Today);
                else
                    predicate = predicate.Or(e => e.Status == MaintenanceContractStatus.Waiting && e.StartDate.Date > DateTime.Today && e.EndDate.Date > DateTime.Today);

                newStatus.Remove(MaintenanceContractStatus.Waiting);
                isFirst = false;
            }

            if (newStatus.Any())
            {
                predicate = predicate.And(e => newStatus.Contains(e.Status));
            }

            return predicate;
        }
    }
}
