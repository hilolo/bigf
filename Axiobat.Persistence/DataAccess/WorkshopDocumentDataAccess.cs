﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access implementation for <see cref="IWorkshopDocumentDataAccess"/>
    /// </summary>
    public partial class WorkshopDocumentDataAccess
    {
        /// <summary>
        /// get the document  of the given workshop
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns>the list of documents</returns>
        public Task<IEnumerable<WorkshopDocument>> GetWorkshopDocumentsAsync(string workshopId)
            => GetList(null).Where(e => e.WorkshopId == workshopId).ToIEnumerableAsync();

        /// <summary>
        /// add the Documents Types
        /// </summary>
        /// <param name="documentsTypes">the documents Types</param>
        /// <returns>the operations result</returns>
        public async Task<Result> AddDocumentsTypesAsync(params WorkshopDocumentsTypes[] documentsTypes)
        {
            try
            {
                await _context.WorkshopDocuments_Types.AddRangeAsync(documentsTypes);
                var result = await _context.SaveChangesAsync();

                if (result <= 0 && documentsTypes.Length > 0)
                {
                    _logger.LogWarning(LogEvent.InsertARangeOfRecoreds, "Failed to add the list of Documents Types, unknown reason");
                    return Result.Failed($"failed to add the range of Documents Types, unknown reason", MessageCode.OperationFailedUnknown);
                }

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError(LogEvent.InsertARangeOfRecoreds, ex, "Failed adding list of Documents Types");
                return Result.Failed($"Failed to add the Documents Types", MessageCode.OperationFailedException, ex);
            }
            finally
            {
                _logger.LogTrace("Detach All Entities from the dbContext");
                _context.DetachAllEntities();
            }
        }

        /// <summary>
        /// delete all documents types
        /// </summary>
        /// <param name="documentId">the id of the document to delete it types</param>
        public Task DeleteDocumentTypesAsync(string documentId)
        {
            var sql = $"DELETE FROM {TablesNames.WorkshopDocumentsTypes} WHERE {nameof(WorkshopDocumentsTypes.DocumentId)} = '{documentId}'";
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return _context.Database.ExecuteSqlCommandAsync(sql);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }

        /// <summary>
        /// get the list of document of the given rubric
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <param name="rubricId">the id of the rubric</param>
        /// <returns>list of documents</returns>
        public Task<IEnumerable<WorkshopDocument>> GetRubricDocumentsAsync(string workshopId, string rubricId)
            => GetList(null).Where(e => e.WorkshopId == workshopId && e.RubricId == rubricId).ToIEnumerableAsync();
    }

    /// <summary>
    /// partial part for <see cref="WorkshopDocumentDataAccess"/>
    /// </summary>
    public partial class WorkshopDocumentDataAccess : DataAccess<WorkshopDocument, string>, IWorkshopDocumentDataAccess
    {
        /// <summary>
        /// create an instant of <see cref="WorkshopDocumentDataAccess"/>
        /// </summary>
        /// <param name="context">the application context</param>
        /// <param name="loggerFactory">the logger factory</param>
        public WorkshopDocumentDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<WorkshopDocument> SetPagedResultFilterOptions<IFilter>(IQueryable<WorkshopDocument> query, IFilter filterOption)
        {
            if (filterOption is WorkshopDocumentFilterOptions filter)
            {
                if (filter.RubricId.IsValid())
                {
                    query = query.Where(e => e.RubricId == filter.RubricId);
                }

                if (filter.WorkshopId.IsValid())
                {
                    query = query.Where(e => e.WorkshopId == filter.WorkshopId);
                }
            }

            return query;
        }

        protected override IQueryable<WorkshopDocument> SetDefaultIncludsForListRetrieve(IQueryable<WorkshopDocument> query)
            => query.Include(e => e.Rubric)
                .Include(e => e.Types)
                    .ThenInclude(e => e.Type);

        protected override IQueryable<WorkshopDocument> SetDefaultIncludsForSingleRetrieve(IQueryable<WorkshopDocument> query)
            => query.Include(e => e.Rubric)
                .Include(e => e.Types)
                    .ThenInclude(e => e.Type);
    }
}
