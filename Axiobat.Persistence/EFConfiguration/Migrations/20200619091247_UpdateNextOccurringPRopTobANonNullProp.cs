﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class UpdateNextOccurringPRopTobANonNullProp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "NextOccurring",
                table: "RecurringDocuments",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "NextOccurring",
                table: "RecurringDocuments",
                nullable: true,
                oldClrType: typeof(DateTime));
        }
    }
}
