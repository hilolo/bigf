﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class ApplicationConfigurationEntityConfigurations : IEntityTypeConfiguration<ApplicationConfiguration>
    {
        public void Configure(EntityTypeBuilder<ApplicationConfiguration> builder)
        {
            builder.ToTable("ApplicationConfigurations");

            // primary key
            builder.HasKey(e => e.ConfigurationType);

            // properties configuration
            builder.Property(e => e.Value)
                .HasColumnType("LONGTEXT");

            builder.Property(e => e.LastModifiedOn)
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
        }
    }
}
