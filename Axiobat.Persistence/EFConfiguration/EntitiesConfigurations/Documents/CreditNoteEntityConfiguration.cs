﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class CreditNoteEntityConfiguration : DocumentEntityConfiguration<CreditNote>, IEntityTypeConfiguration<CreditNote>
    {
        public override void Configure(EntityTypeBuilder<CreditNote> builder)
        {
            // add base configuration
            base.Configure(builder);

            // table name
            builder.ToTable("CreditNotes");

            // properties

            // converters
            builder.Property(e => e.OrderDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<WorkshopOrderDetails>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Client)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ClientDocument>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Emails)
               .HasConversion(
                   entity => entity.ToJson(false, false),
                   json => json.FromJson<ICollection<DocumentEmail>>()??new List<DocumentEmail>())
               .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memos)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Memo>>())
                 .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne(e => e.Invoice)
                .WithMany(e => e.CreditNotes)
                .HasForeignKey(e => e.InvoiceId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.Workshop)
               .WithMany(e => e.CreditNotes)
               .HasForeignKey(e => e.WorkshopId)
               .IsRequired(false)
               .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne<Client>()
                .WithMany(e => e.CreditNotes)
                .HasForeignKey(e => e.ClientId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
