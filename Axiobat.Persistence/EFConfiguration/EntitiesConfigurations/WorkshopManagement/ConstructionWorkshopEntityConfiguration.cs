﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class ConstructionWorkshopEntityConfiguration : IEntityTypeConfiguration<ConstructionWorkshop>
    {
        public void Configure(EntityTypeBuilder<ConstructionWorkshop> builder)
        {
            // table name
            builder.ToTable("ConstructionWorkshops");

            // properties configuration
            builder.Property(e => e.Description)
                .HasMaxLength(500);

            builder.Property(e => e.Comment)
                .HasMaxLength(250);

            builder.Property(e => e.Name)
                .HasMaxLength(100);

            builder.HasQueryFilter(e => !e.IsDeleted);

            // converters
            builder.Property(e => e.ChangesHistory)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<List<ChangesHistory>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Client)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ClientDocument>())
                 .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne<Client>()
                .WithMany(e => e.ConstructionWorkshops)
                .HasForeignKey(e => e.ClientId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}