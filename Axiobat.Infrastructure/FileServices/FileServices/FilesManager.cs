﻿namespace FileService.Internal
{
    using Microsoft.Extensions.Logging;
    using SharedServices.FileServices.Models;
    using SharedServices.Responses;
    using System;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// the file manager
    /// </summary>
    public partial class FilesManager
    {
        /// <summary>
        /// check if the file exist or not
        /// </summary>
        /// <param name="fileId">the id of the file</param>
        /// <param name="path">the path</param>
        /// <returns>true if exist false if not</returns>
        internal bool IsExist(string fileId, string path)
        {
            // Encoding to hexadecimal
            var fileNameInHex = EncodingWordToHex(fileId);

            // Get Name of Directory and SubDirectory
            var Directory = GetDirectory(fileNameInHex, _start_directory, _directory_name_length);
            var SubDirectory = GetDirectory(fileNameInHex, _start_sub_directory, _directory_name_length);

            // Path File
            var Path_File = BuildPath(new string[] { path, Directory, SubDirectory, fileId });

            return File.Exists(Path_File);
        }

        /// <summary>
        /// this method is used to save a file in random generated location using the checksum algorithm
        /// </summary>
        /// <param name="base64">the file encoded in base64 format</param>
        /// <param name="fileId">a unique id generated for the file</param>
        /// <returns>the operation result</returns>
        public Task<FileResult> SaveAsync(string fileId, string base64, string path)
        {
            return Task.Run(() =>
            {
                try
                {
                    // Encoding to hexadecimal
                    var fileNameInHex = EncodingWordToHex(fileId);

                    // Get Name of Directory and SubDirectory
                    var Directory = GetDirectory(fileNameInHex, _start_directory, _directory_name_length);
                    var SubDirectory = GetDirectory(fileNameInHex, _start_sub_directory, _directory_name_length);

                    // Create directory
                    CreateDirectory(BuildPath(new string[] { path, Directory }), BuildPath(new string[] { path, Directory, SubDirectory }));

                    // Save File
                    File.WriteAllText(BuildPath(new string[] { path, Directory, SubDirectory, fileId }), base64);

                    // all done
                    return Successed(fileId);
                }
                catch (System.Security.SecurityException ex)
                {
                    _logger.LogError(ex, "Failed to save the file with [id= {fileId}] and base64 content {fileContent}]", fileId, base64);
                    return Failed(fileId, "Failed to save file, The caller does not have the required permission.");
                }
                catch (NotSupportedException ex)
                {
                    _logger.LogError(ex, "Failed to save the file with [id= {fileId}] and base64 content {fileContent}]", fileId, base64);
                    return Failed(fileId, "Failed to save file, path is in an invalid format.");
                }
                catch (UnauthorizedAccessException ex)
                {
                    _logger.LogError(ex, "Failed to save the file with [id= {fileId}] and base64 content {fileContent}]", fileId, base64);
                    return Failed(fileId, "Failed to save file, path specified a file that is read-only. -or- This operation is not supported on the current platform. -or- path specified a directory. -or- The caller does not have the required permission.");
                }
                catch (DirectoryNotFoundException ex)
                {
                    _logger.LogError(ex, "Failed to save the file with [id= {fileId}] and base64 content {fileContent}]", fileId, base64);
                    return Failed(fileId, "Failed to save file, the requested directory not exist");
                }
                catch (PathTooLongException ex)
                {
                    _logger.LogError(ex, "Failed to save the file with [id= {fileId}] and base64 content {fileContent}]", fileId, base64);
                    return Failed(fileId, "Failed to save file, the generated path is too long");
                }
                catch (IOException ex)
                {
                    _logger.LogError(ex, "Failed to save the file with [id= {fileId}] and base64 content {fileContent}]", fileId, base64);
                    return Failed(fileId, "Failed to save file, An I/O error occurred while opening the file.");
                }
                catch (ArgumentNullException ex)
                {
                    _logger.LogError(ex, "Failed to save the file with [id= {fileId}] and base64 content {fileContent}]", fileId, base64);
                    return Failed(fileId, "Failed to save file, the path or content is empty");
                }
                catch (ArgumentException ex)
                {
                    _logger.LogError(ex, "Failed to save the file with [id= {fileId}] and base64 content {fileContent}]", fileId, base64);
                    return Failed(fileId, "Failed to save file path is not in a correct format");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Failed to save the file with [id= {fileId}] and base64 content {fileContent}]", fileId, base64);
                    return Failed(fileId, "Failed to save file, an exception has been thrown");
                }
            });
        }

        /// <summary>
        /// get the file with the given name
        /// </summary>
        /// <param name="fileId">the id of the file to be retrieved</param>
        /// <returns>the file</returns>
        public Task<FileRetrievalResult> GetAsync(string fileId, string path)
        {
            return Task.Run(() =>
            {
                try
                {
                    // Encoding to hexadecimal
                    var fileNameInHex = EncodingWordToHex(fileId);

                    // Get Name of Directory and SubDirectory
                    var Directory = GetDirectory(fileNameInHex, _start_directory, _directory_name_length);
                    var SubDirectory = GetDirectory(fileNameInHex, _start_sub_directory, _directory_name_length);

                    // Path File
                    var Path_File = BuildPath(new string[] { path, Directory, SubDirectory, fileId });

                    // Convert file to Base64
                    var content = File.ReadAllText(Path_File);

                    // all done
                    return FileRetrievalResult.Successed(fileId, content);
                }
                catch (System.Security.SecurityException ex)
                {
                    _logger.LogError(ex, "Failed to retrieve the file with [id= {fileId}]", fileId);
                    return FileRetrievalResult.Failed(fileId, "Failed to save file, The caller does not have the required permission.");
                }
                catch (NotSupportedException ex)
                {
                    _logger.LogError(ex, "Failed to retrieve the file with [id= {fileId}]", fileId);
                    return FileRetrievalResult.Failed(fileId, "Failed to save file, path is in an invalid format.");
                }
                catch (UnauthorizedAccessException ex)
                {
                    _logger.LogError(ex, "Failed to retrieve the file with [id= {fileId}]", fileId);
                    return FileRetrievalResult.Failed(fileId, "Failed to save file, path specified a file that is read-only. -or- This operation is not supported on the current platform. -or- path specified a directory. -or- The caller does not have the required permission.");
                }
                catch (DirectoryNotFoundException ex)
                {
                    _logger.LogError(ex, "Failed to retrieve the file with [id= {fileId}]", fileId);
                    return FileRetrievalResult.Failed(fileId, "Failed to save file, the requested directory not exist");
                }
                catch (PathTooLongException ex)
                {
                    _logger.LogError(ex, "Failed to retrieve the file with [id= {fileId}]", fileId);
                    return FileRetrievalResult.Failed(fileId, "Failed to save file, the generated path is too long");
                }
                catch (IOException ex)
                {
                    _logger.LogError(ex, "Failed to retrieve the file with [id= {fileId}]", fileId);
                    return FileRetrievalResult.Failed(fileId, "Failed to save file, An I/O error occurred while opening the file.");
                }
                catch (ArgumentNullException ex)
                {
                    _logger.LogError(ex, "Failed to retrieve the file with [id= {fileId}]", fileId);
                    return FileRetrievalResult.Failed(fileId, "Failed to save file, the path or content is empty");
                }
                catch (ArgumentException ex)
                {
                    _logger.LogError(ex, "Failed to retrieve the file with [id= {fileId}]", fileId);
                    return FileRetrievalResult.Failed(fileId, "Failed to save file path is not in a correct format");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Failed to retrieve the file with [id= {fileId}]", fileId);
                    return FileRetrievalResult.Failed(fileId, "Failed to save file, an exception has been thrown");
                }
            });
        }

        /// <summary>
        /// delete the file with the given id
        /// </summary>
        /// <param name="fileId">the id of the file to be deleted</param>
        /// <returns>the operation result</returns>
        public Task<FileResult> DeleteAsync(string fileId, string path)
        {
            return Task.Run(() =>
            {
                try
                {
                    // Encoding to hexadecimal
                    var fileNameInHex = EncodingWordToHex(fileId);

                    // Get Name of Directory and SubDirectory
                    var Directory = GetDirectory(fileNameInHex, _start_directory, _directory_name_length);
                    var SubDirectory = GetDirectory(fileNameInHex, _start_sub_directory, _directory_name_length);

                    // Path File
                    var Path_File = BuildPath(new string[] { path, Directory, SubDirectory, fileId });

                    // check if the file exist
                    if (!File.Exists(Path_File))
                        return Failed(fileId, "Failed to deleted the file, Not found");

                    File.Delete(Path_File);
                    return Successed(fileId);
                }
                catch (System.Security.SecurityException ex)
                {
                    _logger.LogError(ex, "Failed to delete the file with [id= {fileId}]", fileId);
                    return Failed(fileId, "Failed to delete file, The caller does not have the required permission.");
                }
                catch (NotSupportedException ex)
                {
                    _logger.LogError(ex, "Failed to delete the file with [id= {fileId}]", fileId);
                    return Failed(fileId, "Failed to delete file, path is in an invalid format.");
                }
                catch (UnauthorizedAccessException ex)
                {
                    _logger.LogError(ex, "Failed to delete the file with [id= {fileId}]", fileId);
                    return Failed(fileId, "Failed to delete file, path specified a file that is read-only. -or- This operation is not supported on the current platform. -or- path specified a directory. -or- The caller does not have the required permission.");
                }
                catch (DirectoryNotFoundException ex)
                {
                    _logger.LogError(ex, "Failed to delete the file with [id= {fileId}]", fileId);
                    return Failed(fileId, "Failed to delete file, the requested directory not exist");
                }
                catch (PathTooLongException ex)
                {
                    _logger.LogError(ex, "Failed to delete the file with [id= {fileId}]", fileId);
                    return Failed(fileId, "Failed to delete file, the generated path is too long");
                }
                catch (IOException ex)
                {
                    _logger.LogError(ex, "Failed to delete the file with [id= {fileId}]]", fileId);
                    return Failed(fileId, "Failed to delete file, An I/O error occurred while opening the file.");
                }
                catch (ArgumentNullException ex)
                {
                    _logger.LogError(ex, "Failed to delete the file with [id= {fileId}]", fileId);
                    return Failed(fileId, "Failed to delete file, the path or content is empty");
                }
                catch (ArgumentException ex)
                {
                    _logger.LogError(ex, "Failed to delete the file with [id= {fileId}]", fileId);
                    return Failed(fileId, "Failed to delete file path is not in a correct format");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Failed to delete the file with [id= {fileId}]", fileId);
                    return Failed(fileId, "Failed to delete file, an exception has been thrown");
                }
            });
        }
    }

    /// <summary>
    /// the partial part for <see cref="FilesManager"/>
    /// </summary>
    public partial class FilesManager
    {
        private const int _start_directory = 0;
        private const int _start_sub_directory = 2;
        private const int _directory_name_length = 2;
        private readonly ILogger _logger;

        public FilesManager(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<FilesManager>();
        }

        private void CreateDirectory(string directory, string subDirectory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            if (!Directory.Exists(subDirectory))
            {
                Directory.CreateDirectory(subDirectory);
            }
        }

        private string EncodingWordToHex(string FileName)
            => FletcherChecksum.GetChecksumHex(FileName, 16);

        private string GetDirectory(string HexFileName, int IndexStart, int Length)
            => HexFileName.Substring(IndexStart, Length);

        private string BuildPath(string[] NameDirectory)
            => string.Join("/", NameDirectory);

        /// <summary>
        /// create a Failed FileSavingStatus
        /// </summary>
        /// <param name="fileId">the id of the file being saved</param>
        /// <returns>the <see cref="FileSavingResult"/> instant</returns>
        private FileResult Failed(string fileId, string reason)
            => new FileResult { Id = fileId, Success = false, Reason = reason };

        /// <summary>
        /// create a success FileSavingStatus
        /// </summary>
        /// <param name="fileId">the id of the file being saved</param>
        /// <returns>the <see cref="FileSavingResult"/> instant</returns>
        private FileResult Successed(string fileId)
            => new FileResult { Id = fileId, Success = true };
    }
}
