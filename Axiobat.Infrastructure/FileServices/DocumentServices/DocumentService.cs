﻿namespace Axiobat.Infrastructure.FileServices
{
    using Application.Services.FileService;
    using Axiobat.Domain.Entities;
    using DocumentFormat.OpenXml.Drawing.Wordprocessing;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Wordprocessing;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    public partial class DocumentService : IDocumentService
    {
        private const string _tagPlaceHolder = @"#(\s|\S)*?#";
        private const string _matchMarkupMicrosoftWord = @"<(w|\/w){1}(\s|\S)*?>";

        /// <summary>
        /// get the tags from the file
        /// </summary>
        /// <param name="stream">the stream</param>
        /// <returns>the list of tags</returns>
        public string[] GetTags(Stream stream)
        {
            var tags = new HashSet<string>();

            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
            {
                var text = wordDoc.MainDocumentPart.Document.InnerText;

                foreach (var item in Regex.Matches(text, $"{_tagPlaceHolder}"))
                    tags.Add(item.ToString());
            }

            return tags.ToArray();
        }

        /// <summary>
        /// replace tags in document
        /// </summary>
        /// <param name="stream">the file in format stream</param>
        /// <param name="tags">the dictionary of tags (tag=> value)</param>
        public void ReplaceContractTags(MemoryStream stream, IDictionary<string, string> tags)
        {
            using (WordprocessingDocument document = WordprocessingDocument.Open(stream, true))
            {
                var paragraphs = document.MainDocumentPart.Document.Descendants<Paragraph>().ToList();

                foreach (var paragraph in paragraphs)
                {
                    var textElements = paragraph.Elements<Run>().SelectMany(run => run.Elements<Text>()).ToArray();

                    foreach (var textElement in textElements)
                    {
                        var parrent = textElement.Parent;

                        foreach (var foundTag in textElement.Text.FindAllMatches(_tagPlaceHolder))
                        {
                            if (tags.TryGetValue(foundTag, out string tagValue))
                            {
                                if (foundTag == $"#{PublishingContract.SignatureTag}#") // should insert the image
                                {
                                    if (Base64ImageData.TryParse(tagValue, out var imageData))
                                    {
                                        var imagePart = document.MainDocumentPart.AddImagePart(imagePartType(imageData));

                                        using (var imageStream = new MemoryStream(imageData.RawData))
                                        {
                                            imagePart.FeedData(imageStream);

                                            var imageElement = GetImageElement(
                                               document.MainDocumentPart.GetIdOfPart(imagePart),
                                               $"signature.{imageData.ImageType}",
                                               "signature",
                                               200,
                                               200);

                                            parrent.InsertBefore(new Run(imageElement), textElement);
                                            textElement.Remove();
                                        }
                                    }
                                }
                                else // should replace the text
                                {
                                    textElement.Text = textElement.Text.Replace(foundTag, tagValue);
                                }
                            }
                            else
                            {
                                textElement.Text = textElement.Text.Replace(foundTag, "");
                            }
                        }
                    }
                }
            }

            stream.Seek(0, SeekOrigin.Begin);

            ImagePartType imagePartType(Base64ImageData imageData)
            {
                switch (imageData.ImageType)
                {
                    case "png": return ImagePartType.Png;
                    case "tiff": return ImagePartType.Tiff;
                    case "jpg": return ImagePartType.Jpeg;
                    case "jpeg": return ImagePartType.Jpeg;
                    case "gif": return ImagePartType.Gif;
                    default:
                        return ImagePartType.Jpeg;
                }
            }
        }

        /// <summary>
        /// get the file as a byte array from the given file path
        /// </summary>
        /// <param name="filePath">the path of the file</param>
        /// <param name="tags">the list of tags to be updated in the document</param>
        /// <returns>the file as a byte array</returns>
        public async Task<byte[]> GetFileAsync(string filePath, IDictionary<string, string> tags)
        {
            if (!File.Exists(filePath))
                return null;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (Stream input = File.OpenRead(filePath))
                {
                    await input.CopyToAsync(memoryStream);
                }

                memoryStream.Position = 0;

                ReplaceContractTags(memoryStream, tags);

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// save the file form the given stream
        /// </summary>
        /// <param name="fileStream">the file stream</param>
        /// <param name="fileFullName">the full name of file (with the path)</param>
        public async Task Async(Stream fileStream, string fileFullName)
        {
            using (var stream = new FileStream(fileFullName, FileMode.Create))
            {
                await fileStream.CopyToAsync(stream);
            }
        }

        /// <summary>
        /// remove file
        /// </summary>
        /// <param name="path">the path of file</param>
        /// <returns></returns>
        public void RemoveFile(string path)
        {
            File.Delete(path);
        }
    }

    public partial class DocumentService
    {
        private static Drawing GetImageElement(
        string imagePartId,
        string fileName,
        string pictureName,
        double width,
        double height)
        {
            double englishMetricUnitsPerInch = 914400;
            double pixelsPerInch = 96;

            //calculate size in emu
            double emuWidth = width * englishMetricUnitsPerInch / pixelsPerInch;
            double emuHeight = height * englishMetricUnitsPerInch / pixelsPerInch;

            var element = new Drawing(
                new Inline(
                    new Extent { Cx = (DocumentFormat.OpenXml.Int64Value)emuWidth, Cy = (DocumentFormat.OpenXml.Int64Value)emuHeight },
                    new EffectExtent { LeftEdge = 0L, TopEdge = 0L, RightEdge = 0L, BottomEdge = 0L },
                    new DocProperties { Id = (DocumentFormat.OpenXml.UInt32Value)1U, Name = pictureName },
                    new DocumentFormat.OpenXml.Drawing.NonVisualGraphicFrameDrawingProperties(
                    new DocumentFormat.OpenXml.Drawing.GraphicFrameLocks { NoChangeAspect = true }),
                    new DocumentFormat.OpenXml.Drawing.Graphic(
                        new DocumentFormat.OpenXml.Drawing.GraphicData(
                            new DocumentFormat.OpenXml.Drawing.Pictures.Picture(
                                new DocumentFormat.OpenXml.Drawing.Pictures.NonVisualPictureProperties(
                                    new DocumentFormat.OpenXml.Drawing.Pictures.NonVisualDrawingProperties { Id = (DocumentFormat.OpenXml.UInt32Value)0U, Name = fileName },
                                    new DocumentFormat.OpenXml.Drawing.Pictures.NonVisualPictureDrawingProperties()),
                                new DocumentFormat.OpenXml.Drawing.Pictures.BlipFill(
                                    new DocumentFormat.OpenXml.Drawing.Blip(
                                        new DocumentFormat.OpenXml.Drawing.BlipExtensionList(
                                            new DocumentFormat.OpenXml.Drawing.BlipExtension { Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}" }))
                                    {
                                        Embed = imagePartId,
                                        CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print
                                    },
                                            new DocumentFormat.OpenXml.Drawing.Stretch(new DocumentFormat.OpenXml.Drawing.FillRectangle())),
                                new DocumentFormat.OpenXml.Drawing.Pictures.ShapeProperties(
                                    new DocumentFormat.OpenXml.Drawing.Transform2D(
                                        new DocumentFormat.OpenXml.Drawing.Offset { X = 0L, Y = 0L },
                                        new DocumentFormat.OpenXml.Drawing.Extents { Cx = (DocumentFormat.OpenXml.Int64Value)emuWidth, Cy = (DocumentFormat.OpenXml.Int64Value)emuHeight }),
                                    new DocumentFormat.OpenXml.Drawing.PresetGeometry(
                                        new DocumentFormat.OpenXml.Drawing.AdjustValueList())
                                    { Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle })))
                        {
                            Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture"
                        }))
                {
                    DistanceFromTop = (DocumentFormat.OpenXml.UInt32Value)0U,
                    DistanceFromBottom = (DocumentFormat.OpenXml.UInt32Value)0U,
                    DistanceFromLeft = (DocumentFormat.OpenXml.UInt32Value)0U,
                    DistanceFromRight = (DocumentFormat.OpenXml.UInt32Value)0U,
                    EditId = "50D07946"
                });
            return element;
        }
    }

}
