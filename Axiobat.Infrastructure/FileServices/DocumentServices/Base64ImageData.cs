﻿namespace Axiobat
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// a base64 decoder helper class used to parse image data from a base 64 string
    /// </summary>
    public sealed class Base64ImageData
    {
        private static readonly Regex DataUriPattern = new Regex(@"^data\:(?<mimeType>image\/(?<imageType>png|tiff|jpg|Jpeg|gif));base64,(?<data>[A-Z0-9\+\/\=]+)$", RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase);

        private Base64ImageData(string mimeType, string imageType, byte[] rawData)
        {
            MimeType = mimeType;
            RawData = rawData;
            ImageType = imageType;
        }

        /// <summary>
        /// the MimeType
        /// </summary>
        public string MimeType { get; }

        /// <summary>
        /// the byte[] data of the base64
        /// </summary>
        public byte[] RawData { get; }

        /// <summary>
        /// the image type
        /// </summary>
        public string ImageType { get; }

        /// <summary>
        /// try parse the base64 string and get the <see cref="Base64ImageData"/>
        /// </summary>
        /// <param name="base64">the base64 value</param>
        /// <param name="image">the base64 image data</param>
        /// <returns>true if parsed successfully false if not</returns>
        public static bool TryParse(string base64, out Base64ImageData image)
        {
            image = null;

            try
            {
                if (string.IsNullOrWhiteSpace(base64))
                    return false;

                var match = DataUriPattern.Match(base64);
                if (!match.Success)
                    return false;

                var mimeType = match.Groups["mimeType"].Value;
                var imageType = match.Groups["imageType"].Value;
                var rawData = System.Convert.FromBase64String(match.Groups["data"].Value);

                image = rawData.Length == 0
                    ? null
                    : new Base64ImageData(mimeType, imageType, rawData);

                return true;
            }
            catch (System.FormatException)
            {
                return false;
            }
        }
    }

}
