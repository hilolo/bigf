﻿namespace Axiobat.Infrastructure.ImportService
{
    using Axiobat.Domain.Entities;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Axiobat.Application.Models.DataImport;
    using Axiobat.Application.Services.FileService;
    using Axiobat.Domain.Entities.Configuration;
    using App.Common;
    using Axiobat.Domain.Constants;
    using Axiobat.Application.Models;
    using System.Globalization;

    public partial class DataImportService : IDataImportService
    {
        public IEnumerable<Client> GetClientsFromFile(
            Stream file,
            int startingRow,
            Numerator clientsNumerator,
            string defaultClientCode,
            ClientColumnsDefinitions columnsDefinitions,
            string worksheetName)
        {
            var clients = new LinkedList<Client>();

            using (var excelPackage = new OfficeOpenXml.ExcelPackage())
            {
                excelPackage.Load(file);

                var worksheet = excelPackage.Workbook.Worksheets
                    .FirstOrDefault(e => e.Name.Equals(worksheetName, StringComparison.OrdinalIgnoreCase));

                if (worksheet is null)
                    throw new ArgumentException($"there is no worksheet with name '{worksheetName}'");

                var rowCount = worksheet.Dimension.Rows;

                for (int row = startingRow; row <= rowCount; row++)
                {
                    var client = new Client();

                    if (columnsDefinitions.Reference.HasValue)
                        client.Reference = worksheet.GetValue<string>(row, columnsDefinitions.Reference.Value);

                    if (!client.Reference.IsValid())
                        client.Reference = clientsNumerator.Generate(true);

                    if (columnsDefinitions.ClientCode.HasValue)
                        client.Code = worksheet.GetValue<string>(row, columnsDefinitions.ClientCode.Value);

                    if (columnsDefinitions.FirstName.HasValue)
                        client.FirstName = worksheet.GetValue<string>(row, columnsDefinitions.FirstName.Value);

                    if (columnsDefinitions.LastName.HasValue)
                        client.LastName = worksheet.GetValue<string>(row, columnsDefinitions.LastName.Value);

                    if (columnsDefinitions.AccountingCode.HasValue)
                        client.AccountingCode = worksheet.GetValue<string>(row, columnsDefinitions.AccountingCode.Value);

                    if (!client.AccountingCode.IsValid())
                        client.AccountingCode = (defaultClientCode + client.FullName)
                            .Replace(" ", string.Empty)
                            .Replace("'", string.Empty);

                    if (columnsDefinitions.PhoneNumber.HasValue)
                        client.PhoneNumber = worksheet.GetValue<string>(row, columnsDefinitions.PhoneNumber.Value);

                    if (columnsDefinitions.Email.HasValue)
                        client.Email = worksheet.GetValue<string>(row, columnsDefinitions.Email.Value);

                    if (columnsDefinitions.Website.HasValue)
                        client.Website = worksheet.GetValue<string>(row, columnsDefinitions.Website.Value);

                    if (columnsDefinitions.Siret.HasValue)
                        client.Siret = worksheet.GetValue<string>(row, columnsDefinitions.Siret.Value);

                    if (columnsDefinitions.IntraCommunityVAT.HasValue)
                        client.IntraCommunityVAT = worksheet.GetValue<string>(row, columnsDefinitions.IntraCommunityVAT.Value);

                    if (columnsDefinitions.ClientType.HasValue)
                        client.Type = worksheet.GetValue<string>(row, columnsDefinitions.ClientType.Value);

                    if (!client.Type.IsValid())
                        client.Type = ClientType.Particular;

                    var address = new Address();
                    if (columnsDefinitions.Address_Designation.HasValue)
                        address.Designation = worksheet.GetValue<string>(row, columnsDefinitions.Address_Designation.Value);

                    if (columnsDefinitions.Address_Street.HasValue)
                        address.Street = worksheet.GetValue<string>(row, columnsDefinitions.Address_Street.Value);

                    if (columnsDefinitions.Address_City.HasValue)
                        address.City = worksheet.GetValue<string>(row, columnsDefinitions.Address_City.Value);

                    if (columnsDefinitions.Address_PostalCode.HasValue)
                        address.PostalCode = worksheet.GetValue<string>(row, columnsDefinitions.Address_PostalCode.Value);

                    if (columnsDefinitions.Address_Country.HasValue)
                        address.CountryCode = worksheet.GetValue<string>(row, columnsDefinitions.Address_Country.Value);

                    client.Id += row;
                    client.Addresses.Add(address);
                    client.BuildSearchTerms();
                    clients.AddLast(client);
                }
            }

            return clients;
        }

        public IEnumerable<Supplier> GetSuppliersFromFile(
            Stream file,
            int startingRow,
            SupplierColumnsDefinitions columnsDefinitions,
            string worksheetName = "suppliers")
        {
            var suppliers = new LinkedList<Supplier>();

            using (var excelPackage = new OfficeOpenXml.ExcelPackage())
            {
                excelPackage.Load(file);

                var worksheet = excelPackage.Workbook.Worksheets.First(e => e.Name == worksheetName);
                if (worksheet is null)
                    throw new ArgumentException($"there is no worksheet with name '{worksheetName}'");

                var rowCount = worksheet.Dimension.Rows;

                for (int row = startingRow; row < rowCount; row++)
                {
                    var Supplier = new Supplier
                    {
                        Reference = worksheet.GetValue<string>(row, columnsDefinitions.Reference),
                        AccountingCode = worksheet.GetValue<string>(row, columnsDefinitions.AccountingCode),
                        FirstName = worksheet.GetValue<string>(row, columnsDefinitions.FirstName),
                        LastName = worksheet.GetValue<string>(row, columnsDefinitions.LastName),
                        PhoneNumber = worksheet.GetValue<string>(row, columnsDefinitions.PhoneNumber),
                        Email = worksheet.GetValue<string>(row, columnsDefinitions.Email),
                        Website = worksheet.GetValue<string>(row, columnsDefinitions.Website),
                        Siret = worksheet.GetValue<string>(row, columnsDefinitions.Siret),
                        IntraCommunityVAT = worksheet.GetValue<string>(row, columnsDefinitions.IntraCommunityVAT),
                        Address = new Address
                        {
                            Designation = worksheet.GetValue<string>(row, columnsDefinitions.Address_Designation),
                            Street = worksheet.GetValue<string>(row, columnsDefinitions.Address_Street),
                            City = worksheet.GetValue<string>(row, columnsDefinitions.Address_City),
                            PostalCode = worksheet.GetValue<string>(row, columnsDefinitions.Address_PostalCode),
                            CountryCode = worksheet.GetValue<string>(row, columnsDefinitions.Address_Country),
                        }
                    };

                    suppliers.AddLast(Supplier);
                }
            }

            return suppliers;
        }


        static float ToFloat(string value)
        {
            try
            {
                CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                ci.NumberFormat.CurrencyDecimalSeparator = ".";
                return float.Parse(value, NumberStyles.Any, ci);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"failed to convert [{value}] to float, {ex.Message}");
                return 0;
            }
        }

        public IEnumerable<Product> GetProductsFromFile(
            Stream file,
            int startingRow,
            User superAdmin,
            ProductColumnsDefinitions columnsDefinitions,
            ExternalPartnerMinimalInfo[] suppliers,
            string worksheetName)
        {
            var products = new LinkedList<Product>();

            using (var excelPackage = new OfficeOpenXml.ExcelPackage())
            {
                excelPackage.Load(file);

                var worksheet = excelPackage.Workbook.Worksheets.First(e => e.Name == worksheetName);
                if (worksheet is null)
                    throw new ArgumentException($"there is no worksheet with name '{worksheetName}'");

                var supplierGroupe = suppliers.ToLookup(e => e.Reference);
                var rowCount = worksheet.Dimension.Rows;

                for (int row = startingRow; row < rowCount; row++)
                {

                    try
                    {
                        var product = new Product
                        {
                            Reference = worksheet.GetValue<string>(row, columnsDefinitions.Reference),
                            Designation = worksheet.GetValue<string>(row, columnsDefinitions.Designation),
                            Unite = worksheet.GetValue<string>(row, columnsDefinitions.Unite),
                            TotalHours = worksheet.GetValue<float>(row, columnsDefinitions.TotalHours),
                            HourlyCost = worksheet.GetValue<float>(row, columnsDefinitions.HourlyCost),
                            MaterialCost = worksheet.GetValue<float>(row, columnsDefinitions.MaterialCost),
                            VAT = worksheet.GetValue<float>(row, columnsDefinitions.VAT),
                            ClassificationId = worksheet.GetValue<int>(row, columnsDefinitions.Classification),
                            ProductCategoryTypeId = worksheet.GetValue<string>(row, columnsDefinitions.ProductCategoryTypeId),
                        };
                        if (columnsDefinitions.PriceSupplier.HasValue && columnsDefinitions.RefSupplier.HasValue)
                        {
                            var supplier = supplierGroupe[worksheet.GetValue<string>(row, columnsDefinitions.RefSupplier.Value)];
                            if (supplier.Any())
                            {
                                var Price = ToFloat(worksheet.GetValue<string>(row, columnsDefinitions.PriceSupplier.Value));
                                product.Suppliers.Add(new ProductSupplier
                                {
                                    Price = Price,
                                    SupplierId = supplier.FirstOrDefault().Id,
                                });
                            }
                        }

                        if (columnsDefinitions.Description.HasValue)
                            product.Description = worksheet.GetValue<string>(row, columnsDefinitions.Description.Value);
                        else
                            product.Description = "";


                        product.BuildSearchTerms();
                        product.ChangesHistory.Add(new ChangesHistory
                        {
                            User = superAdmin,
                            Action = Domain.Enums.ChangesHistoryType.Added,
                            Information = "Import Articles"
                        });

                        product.Id += row;



                        products.AddLast(product);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("=>" + row);
                        throw;
                    }
                }
            }

            return products;
        }

        public IEnumerable<ProductPricing> GetProductPricingsFromFile(
            Stream file,
            int startingRow,
            ProductPriceColumnsDefinitions columnsDefinitions,
            string worksheetName = "Product Price Per Supplier")
        {
            var pricing = new LinkedList<ProductPricing>();

            using (var excelPackage = new OfficeOpenXml.ExcelPackage())
            {
                excelPackage.Load(file);

                var worksheet = excelPackage.Workbook.Worksheets.First(e => e.Name == worksheetName);
                if (worksheet is null)
                    throw new ArgumentException($"there is no worksheet with name '{worksheetName}'");

                var rowCount = worksheet.Dimension.Rows;

                for (int row = startingRow; row < rowCount; row++)
                {
                    var productPricing = new ProductPricing
                    {
                        ProductReference = worksheet.GetValue<string>(row, columnsDefinitions.ProductReference),
                        SupplierReference = worksheet.GetValue<string>(row, columnsDefinitions.SupplierReference),
                        Price = worksheet.GetValue<float>(row, columnsDefinitions.Price),
                        IsDefault = worksheet.GetValue<bool>(row, columnsDefinitions.IsDefault),
                    };

                    pricing.AddLast(productPricing);
                }
            }

            return pricing;
        }
    }

    /*

     var clientColumnsDefinitions = new ClientColumnsDefinitions
            {
                Reference = 1,
                AccountingCode = 2,
                ClientCode = 3,
                FirstName = 4,
                LastName = 5,
                PhoneNumber = 6,
                Email = 7,
                Website = 8,
                Siret = 9,
                IntraCommunityVAT = 10,
                ClientType = 11,
                Address_Designation = 12,
                Address_Street = 13,
                Address_City = 14,
                Address_PostalCode = 15,
                Address_Country = 16,
            };
            var supplierColumnsDefinitions = new SupplierColumnsDefinitions
            {
                Reference = 1,
                AccountingCode = 2,
                FirstName = 3,
                PhoneNumber = 4,
                Email = 5,
                Website = 6,
                Siret = 7,
                IntraCommunityVAT = 8,
                Address_Street = 9,
                Address_City = 10,
                Address_PostalCode = 11,
                Address_Country = 12,
            };
            var productColumnsDefinitions = new ProductColumnsDefinitions
            {
                Reference = 1,
                Name = 2,
                Description = 3,
                Designation = 4,
                Unite = 5,
                TotalHours = 6,
                HourlyCost = 7,
                MaterialCost = 8,
                VAT = 9,
                Classification = 10
            };
            var productPriceColumnsDefinitions = new ProductPriceColumnsDefinitions
            {
                ProductReference = 1,
                SupplierReference = 2,
                Price = 3,
                IsDefault = 4,
            };
     */
}
