﻿namespace Axiobat.Test
{
    using System;
    using Xunit;

    public class DateHelperTestShould
    {
        [Fact]
        public void Get_Start_Of_Week_1()
        {
            var date = new DateTime(2020, 2, 28, 10, 12, 33);
            var startOfWeek = date.StartOfWeek(DayOfWeek.Monday);
            Assert.Equal(new DateTime(2020, 2, 24), startOfWeek);
        }

        [Fact]
        public void Get_Start_Of_Week_2()
        {
            var date = new DateTime(2020, 4, 1, 10, 12, 33);
            var startOfWeek = date.StartOfWeek(DayOfWeek.Monday);

            Assert.Equal(new DateTime(2020, 3, 30), startOfWeek);
        }

        [Fact]
        public void Get_End_Of_Week_1()
        {
            var date = new DateTime(2020, 2, 28, 10, 12, 33);
            var endOfWeek = date.EndOfWeek(DayOfWeek.Monday);

            Assert.Equal(new DateTime(2020, 3, 1), endOfWeek);
        }

        [Fact]
        public void Get_End_Of_Week_2()
        {
            var date = new DateTime(2020, 4, 1, 10, 12, 33);
            var endOfWeek = date.EndOfWeek(DayOfWeek.Monday);

            Assert.Equal(new DateTime(2020, 4, 5), endOfWeek);
        }

        [Fact]
        public void Get_Day_of_Week_Date_1()
        {
            var date = new DateTime(2020, 4, 1, 10, 12, 33);
            var dayOfWeekDate = date.DayOfWeekDate(DayOfWeek.Monday);
            var startOfWeek = date.StartOfWeek(DayOfWeek.Monday);

            Assert.Equal(startOfWeek, dayOfWeekDate);
        }

        [Fact]
        public void Get_Day_of_Week_Date_2()
        {
            var date = new DateTime(2020, 4, 1, 10, 12, 33);
            var dayOfWeekDate = date.DayOfWeekDate(DayOfWeek.Wednesday);

            Assert.Equal(new DateTime(2020, 4, 1), dayOfWeekDate);
        }

        [Fact]
        public void Get_Day_of_Week_Date_3()
        {
            var date = new DateTime(2020, 4, 1, 10, 12, 33);
            var dayOfWeekDate = date.DayOfWeekDate(DayOfWeek.Sunday);
            var dayOfWeekDate2 = date.DayOfWeekDate(DayOfWeek.Saturday);
            var endOfWeek = date.EndOfWeek();

            Assert.Equal(endOfWeek, dayOfWeekDate);
            Assert.Equal(endOfWeek.AddDays(-1), dayOfWeekDate2);
        }
    }
}
